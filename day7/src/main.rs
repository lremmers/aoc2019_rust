use intcode;

use std::fs;

fn main() {
	let input = fs::read_to_string("day7_input.txt").unwrap();
	let program: Vec<i64> = input
					.trim()
					.split(',')
					.enumerate()
					.map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();
    let mut thrust = 0;
    let mut endcode = vec![0,0,0,0,0];
    for n1 in 0..5 {
        for n2 in 0..5 {
            if n2 == n1 {continue;}
            for n3 in 0..5 {
                if n3 == n2 || n3 == n1 { continue; }
                for n4 in 0..5 {
                    if n4 == n3 || n4 == n2 || n4 == n1 { continue ;}
                    for n5 in 0..5 {
                        if n5 == n4 || n5 == n3 || n5 == n2 || n5 == n1 { continue; }
                        let code = vec![n1, n2, n3, n4, n5];
                        let output = get_thruster_value(program.clone(), &code);
                        if output > thrust {
                            thrust = output;
                            endcode = code;
                        }
                    }
                }
            }    
        }
    }
    println!("Maximum thrust {} with phasecode {:?}", thrust, endcode);

    let mut thrust = 0;
    let mut endcode = vec![0,0,0,0,0];
    for n1 in 5..10 {
        for n2 in 5..10 {
            if n2 == n1 {continue;}
            for n3 in 5..10 {
                if n3 == n2 || n3 == n1 { continue; }
                for n4 in 5..10 {
                    if n4 == n3 || n4 == n2 || n4 == n1 { continue ;}
                    for n5 in 5..10 {
                        if n5 == n4 || n5 == n3 || n5 == n2 || n5 == n1 { continue; }
                        let code = vec![n1, n2, n3, n4, n5];
                        let output = get_thruster_value2(program.clone(), &code);
                        if output > thrust {
                            thrust = output;
                            endcode = code;
                        }
                    }
                }
            }    
        }
    
    }
    println!("Maximum thrust {} with phasecode {:?}", thrust, endcode);
}



fn get_thruster_value(program: Vec<i64>, phasecodes: &Vec<i64>) -> i64 {
    let mut output = 0;
    for (_n, phase) in phasecodes.iter().enumerate() {
        let input = output;
        let mut computer = intcode::Intcode::new(program.clone(), vec![*phase, input]);
        let result = computer.run();
        match result {
            Ok(_value)   => output = computer.outputs[0],
            Err(err) => if err.to_string() == "Halted" {
                           output = computer.outputs[0];
                        } else {
                            panic!("No valid output");
                        }   
        };
        //println!("output {} with input {} and phasecode {}: {}", n, input, phase, output)
    }
    output
}

fn get_thruster_value2(program: Vec<i64>, phasecodes: &Vec<i64>) -> i64 {
    let mut amplifiers : Vec<intcode::Intcode> = vec![];
    for phase in phasecodes.iter() {
        amplifiers.push(intcode::Intcode::new(program.clone(), vec![*phase]));
    }
    let mut thrust = 0;
    let mut output = 0;
    let mut end = false;
    let mut count = 50;
    while !end && count > 0 {
        for n in 0..5 {
            let input = output;
            amplifiers[n].push_in(input);
            let result = amplifiers[n].run();
            match result {
                Ok(value) => { 
                    end = value;
                    output = *amplifiers[n].outputs.last().unwrap();
                },
                Err(err) => if err.to_string() == "Halted" {
                                output = *amplifiers[n].outputs.last().unwrap();
                                end = true;
                            } else {
                                panic!("No valid output");
                            }   
            };
            //println!("output {} with input {}: {}", n, input, output);
            if n == 4 {
                thrust = output;
            }
        }
        count -= 1;
    }
    thrust  
}

#[test]
fn day7_1_example1() {
    let program = vec![3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0];
    let phasecodes = vec![4,3,2,1,0];
    let output = get_thruster_value(program, &phasecodes);
    assert_eq!(output, 43210);
}

#[test]
fn day7_1_example2() {
    let program = vec![3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0];
    let phasecodes = vec![0,1,2,3,4];
    let output = get_thruster_value(program, &phasecodes);
    assert_eq!(output, 54321);
}

#[test]
fn day7_1_example3() {
    let program = vec![3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0];
    let phasecodes = vec![1,0,4,3,2];
    let output = get_thruster_value(program, &phasecodes);
    assert_eq!(output, 65210);
}

#[test]
fn day7_2_example1() {
    let program = vec![3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5];
    let phasecodes = vec![9,8,7,6,5];
    let output = get_thruster_value2(program, &phasecodes);
    assert_eq!(output, 139629729);
}

#[test]
fn day7_2_example2() {
    let program = vec![3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
                      -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
                      53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10];
    let phasecodes = vec![9,7,8,5,6];
    let output = get_thruster_value2(program, &phasecodes);
    assert_eq!(output, 18216);
}
