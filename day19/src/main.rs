use std::collections::HashMap;
use std::fs;
use std::fmt;
use std::io;
use std::io::Write;

const XMAX: usize = 50;
const YMAX: usize = 50;

fn main() {
    let input = fs::read_to_string("day19_input.txt").unwrap();
    let program: Vec<i64> = input
        .trim()
        .split(',')
        .enumerate()
        .map(|(i,s)| s.parse().expect(&format!("Parsing program failed at {}: '{}'", i, s)))
        .collect();

    // Intcode really should be modified to accept a &mut program, to prevent all this cloning
    part1(program.clone());
    part2(program.clone());
}


/// Part 1 scans a 50x50 sized part of space, originating at (0,0), deploys the drone to it
/// and reads back its tracting/not tracting read-out
/// It counts the number of locations where tracting was observed.
fn part1(program: Vec<i64>) {

    let mut map: HashMap<Location, bool> = HashMap::new();

    for y in 0..YMAX {
        for x in 0..XMAX {
            let (loc, tracting) = deploy_drone(x, y, program.clone());
            map.insert(loc, tracting);
        }
    }
    print_map(&map, XMAX, YMAX, None, 0);
    let count = map.iter()
                   .filter(|(_k,v)| **v )
                   .count();
    println!("{} points affected by the tractor beam.", count);
}

/// Part 2 scans a growing part of space, trying to find the closest 
/// part of space where a 10x10 object would be completely immersed in
/// the tractor beam
/// A growing square around tge origin is scanned.
/// The first and last tracting point on each border walk is stored. 
/// This yields a row of left and a right edge points.
/// Every time a left edge point is stored, it is tested if the opposite
/// top right point is already available.
/// Every time a right edge point is stored, it is tested if the opposite
/// bottom left point is available.
/// If any of these tests is successfull, the top left point, and the answer, are printed. 
fn part2(program: Vec<i64>) {

    let mut map: HashMap<Location, bool> = HashMap::new();
    let mut area = Area::new();
    let mut sol = Part2SolutionState::new(100);

    loop {
        let (loc, tracting) = deploy_drone(area.x, area.y, program.clone());
        // print!("{}:{}/", loc, tracting);
        map.insert(loc, tracting);
        if let Some(result) = sol.check(&map, &area) {
            println!("100x100 square atractable at {}", result);            // we have traction
            println!("Answer = {}", result.x*10000 + result.y);
            print_map(&map, area.size, area.size, Some(result), sol.size);
            println!("100x100 square atractable at {}", result);            // we have traction
            println!("Answer = {}", result.x*10000 + result.y);
            return;
        }
        area.walk_border();
    }
}

fn deploy_drone(x: usize, y: usize, program: Vec<i64>) -> (Location, bool) {
    let mut computer = intcode::Intcode::new(program, vec![x as i64, y as i64]);
    let _halted = computer.run().unwrap();
    let res = computer.outputs.pop().unwrap();
    let loc = Location::new(x as usize, y as usize);
    (loc, if res == 1 { true } else { false })
}

#[derive(Debug, PartialEq, Hash, Copy, Clone, Eq)]
struct Location {
    x: usize,
    y: usize,
}

impl Location {
    fn new(x: usize, y: usize) -> Self {
        Location { x, y }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

struct Area {
    x: usize,
    y: usize,
    size: usize,
}

impl Area {
    fn new() -> Area {
        Area { x: 0, y: 0, size: 1 }
    }

    fn walk_border(&mut self) ->  Location {
        if self.x == 0 {
            // End of circumference found
            self.size += 1;
            self.y = 0;              // Start at top right corner
            self.x = self.size-1;
            print!("\r{}", self.size);
            io::stdout().flush(); 
        } else if self.y < self.size-1 {
            self.y += 1;             // Walking down at right side
        } else {
            self.x -= 1;             // Walking lef at bottom side
        }
        Location::new( self.x,  self.y )
    }
}

/// Holds the state for finding a solutin for part 2
/// Call this every step, or only after completing a border walk (i.e. x == 0)
/// The leftmost (x,y) of the first row with size tracting x's is stored.
/// Every time a new row cannot hold those 100 positions below that, 
/// the first point is updated
/// When row number size is found, the (x,y) is returned
/// 
struct Part2SolutionState {
    size: usize,
    edge_r: Vec<Location>,
    edge_l: Vec<Location>,
}

impl Part2SolutionState {
    fn new( size: usize ) -> Self {
        Part2SolutionState { size, edge_r: vec![], edge_l: vec![] }
    }

    fn check(&mut self, map: &HashMap<Location, bool>, area: &Area) -> Option<Location> {
        if area.x != 0 {
            return None
        }
        // println!();
        // println!("Size: {}", area.size);
        let mut last_tract = false;
        let mut border:  Vec<Location> = (0..(area.size))
                                        .map(|y| Location::new(area.size-1, y))
                                        .collect();
        let mut border2 = (0..(area.size))
                        .rev()
                        .skip(1)
                        .map(|x| Location::new(x, area.size-1))
                        .collect::<Vec<_>>();
        border.append(&mut border2);
        // for b in border.iter() {
        //     print!("{},", b);
        // }
        // println!();
                        
        for loc in border {
            let this_tract = map[&loc];
            if this_tract && !last_tract {
                // println!("{} tract", loc);
                self.edge_r.push(loc);
                if let Some(loc2) = self.test_r(loc, map){
                    return Some(loc2)
                }
            }
            if !this_tract && last_tract {
                // println!("{} untract", loc);
                self.edge_l.push(loc);
                if let Some(loc2) = self.test_l(loc, map){
                    return Some(loc2)
                }
            }
            last_tract = this_tract;
        }
        None
    }

    fn test_r(&mut self, loc: Location, map: &HashMap<Location, bool> ) -> Option<Location> {
        if loc.x >= self.size {
            if let Some(tract) = map.get(&Location::new(loc.x-(self.size-1), loc.y+(self.size-1)) ) {
                if *tract {
                    return Some(Location::new(loc.x-(self.size-1), loc.y))
                } 
            }
        }
        None
    }

    fn test_l(&mut self, loc: Location, map: &HashMap<Location, bool> ) -> Option<Location> {
        if loc.y >= self.size {
            if let Some(tract) = map.get(&Location::new(loc.x+(self.size-1), loc.y-(self.size-1)) ) {
                if *tract {
                    return Some(Location::new(loc.x, loc.y - (self.size-1)))
                } 
            }
        }
        None
    }
}


fn print_map(map: &HashMap<Location, bool>, xmax: usize, ymax:usize, object: Option<Location>, sz: usize) {
    for y in 0..ymax {
        for x in 0..xmax {
            let loc = Location::new(x, y);
            match map.get(&loc) {
                None => print!(" "),
                Some(false) => {
                    if loc.y == 0 {
                        print!("{}", getnr(loc.x));
                    } else if loc.x == 0 {
                        print!("{}", getnr(loc.y));
                    } else {
                        print!(".");
                    }
                },
                Some(true) => print_beam(loc, object, sz),
                _  => print!("#"),
            }
        }
        println!();
    }
}

fn getnr(nr: usize) -> usize {
    if nr == 0 {
        nr
    } else if nr % 10 != 0 {
        nr % 10
    } else {
        getnr(nr / 10)
    }
}

fn print_beam(loc: Location, object: Option<Location>, sz: usize) {
    if let Some(obj) = object {
        if loc.x >= obj.x 
           && loc.x < obj.x + sz
           && loc.y >= obj.y
           && loc.y < obj.y + sz {
            print!("O");
            return;
        }
    }
    print!("#");
}