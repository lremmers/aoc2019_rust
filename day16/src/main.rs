use std::fs;
use std::time::Instant;

fn main() {
    let inputstr = fs::read_to_string("day16_input.txt").unwrap();
    let input = inputstr.trim();

    print!("Part 1: ");
    let result = part1(input);
    println!("{}", result);

    print!("Part 2: ");
    let result = part2(input);
    println!("{}", result);
    
}

fn part1(inputstr: &str) -> i32 {
    println!("Size of input: {}", inputstr.len());
    let result = fft1(&inputstr, 100);
    digits2number(&result[0..8])
}

fn part2(inputstr: &str) -> i32 {
    let offset = inputstr[..7].parse::<usize>().unwrap();
    let input = inputstr.repeat(10000);
    let now = Instant::now();
    let result = fft2(&input, offset);
    println!("Needed {:.3} sec.", now.elapsed().as_secs_f32());
    digits2number(&result[..8])
}

fn digits2number(digits: &[i32]) -> i32 {
    digits.iter().fold(0, |acc, d| 10 * acc + d)
}

// Cycle the input cycle times.
fn fft1(inputstr: &str, cycles: usize) -> Vec<i32> {

    let mut input: Vec<_> = inputstr.chars().map(|c| c.to_digit(10).unwrap() as i32).collect();
    for _ in 0..cycles {
        input = fft1_once(input);
    }   
    input
}

fn fft1_once(input: Vec<i32>) -> Vec<i32> {
    let depth = input.len();
    let mut output: Vec<i32> = Vec::with_capacity(depth);
    for index in 1..depth+1 {
        // let adds:i32 = input.iter().enumerate().filter(|(n,_)|((n+1)/index) & 0x3 == 1).map(|(_,e)|e).sum();
        // let subs:i32 = input.iter().enumerate().filter(|(n,_)|((n+1)/index) & 0x3 == 3).map(|(_,e)|e).sum();
        // let single = ((adds - subs) % 10).abs();

        // This does the same as the commented code above but a lot faster:
        // - no division and modulo to find an pattern item.
        // - skip all input items that would be zeroed anyway.
        let mut single = 0_i64;
        let mut n = index-1;                  // start of add group
        while n <= depth {                       // while still in valid indices... 
            for i in n..depth.min(n+index) {    // walk to end of add group
                single += input[i] as i64;              // and add the entries
            }
            for i in n+2*index..depth.min(n+3*index) {      // walk from start to end of subtract group 
                single -= input[i] as i64;                  // and substract all substracting
            }
            n += 4*index                        // Start of next add group
        }

        output.push((single % 10).abs() as i32);
    }
    output
}
 
// Clue from the internet: https://medium.com/@rudyzio92/my-journey-at-advent-of-code-2019-with-c-dd2acecc4a0c
// The last digit is always the same: only the last digit of the pattern is 1
// The second to last digit has an all 0s pattern except,  the last two digits: 1s 
// etc.	
// So for a message of M and an offset of N you need only to calculate M-N digits, starting at N.
// And you only need to add up.
// to calculate digit n in N..M: sum(input(n..M).
// Do that 100 times.

// if offset is given (i.e. not 0),  print the 8 digit number at the indicated offset
fn fft2(inputstr: &str, offset: usize) -> Vec<i32> {
    println!("Size of input: {}", inputstr.len());
    println!("Offset: {}", offset);
    let mut input: Vec<_> = inputstr.chars().skip(offset).map(|c| c.to_digit(10).unwrap() as i32).collect();
    println!("Input condidered from {} to {}: {} items", offset, offset + input.len(), input.len());
    for _ in 0..100 {
        input = fft2_once(input);
    }   
    input
}

fn fft2_once(input: Vec<i32>) -> Vec<i32> {
    let mut sum: i32 = 0;
    let mut output: Vec<i32> = Vec::with_capacity(input.len());
    for inp in input.iter().rev() {
        sum += inp;
        let digit = (sum % 10).abs();
        output.push(digit);
    }
    output.reverse();
    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
       fn test1_1() {
        let input = "12345678";
        let result = fft1(&input, 1);
        assert_eq!(digits2number(&result[0..8]), 48226158);
    }

    #[test]
       fn test1_2() {
        let input = "12345678";
        let result = fft1(&input, 2);
        assert_eq!(digits2number(&result[0..8]), 34040438);
    }

    #[test]
       fn test1_3() {
        let input = "12345678";
        let result = fft1(&input, 3);
        assert_eq!(digits2number(&result[0..8]), 03415518);
    }

    #[test]
       fn test1_4() {
        let input = "12345678";
        let result = fft1(&input, 4);
        assert_eq!(digits2number(&result[0..8]), 01029498);
    }

    #[test]
       fn test2_1() {
        let input = "80871224585914546619083218645595";
        let result = part1(&input);
        assert_eq!(result, 24176176 );
    }

    #[test]
       fn test2_2() {
        let input = "19617804207202209144916044189917";
        let result = part1(&input);
        assert_eq!(result, 73745418);
    }

    #[test]
       fn test2_3() {
        let input = "69317163492948606335995924319873";
        let result = part1(&input);
        assert_eq!(result, 52432133);
    }

    #[test]
       fn test3_1() {
        let input = "03036732577212944063491565474664";
        let result = part2(&input);
        assert_eq!(result, 84462026);
    }

    #[test]
       fn test3_2() {
        let input = "02935109699940807407585447034323";
        let result = part2(&input);
        assert_eq!(result, 78725270);
    }

    #[test]
       fn test3_3() {
        let input = "03081770884921959731165446850517";
        let result = part2(&input);
        assert_eq!(result, 53553731);
    }

}
