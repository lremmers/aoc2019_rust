use intcode;

use std::fs;

fn main() {
    let input = fs::read_to_string("day9_input.txt").unwrap();
	let program: Vec<i64> = input
					.trim()
					.split(',')
					.enumerate()
					.map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();

    let mut computer =  intcode::Intcode::new(program.clone(), vec![1 as i64]);
    let result = computer.run();
    let output = match result {
        Ok(_value) =>  &(computer.outputs),
        Err(err)  => if err.to_string() == "Halted" {
                       &(computer.outputs)
                    } else {
                        panic!("No valid output");
                    }   
    };
    println!("Ended with result: {:?}", output);

    let mut computer =  intcode::Intcode::new(program.clone(), vec![2 as i64]);
    let result = computer.run();
    let output = match result {
        Ok(_value) => &(computer.outputs),
        Err(err)  => if err.to_string() == "Halted" {
                       &(computer.outputs)
                    } else {
                        panic!("No valid output");
                    }   
    };
    println!("Ended with result: {:?}", output);

}


#[test]
fn day9_1_example1() {//0                 5                 10                15
    let program = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
    let prog = program.clone();
    let inputs: Vec<i64> = vec![];
    let mut computer =  intcode::Intcode::new(program, inputs);
    let result = computer.run();
    let output = match result {
        Ok(_value) => &(computer.outputs),
        Err(err)  => if err.to_string() == "Halted" {
                       &(computer.outputs)
                    } else {
                        panic!("No valid output");
                    }   
    };
    println!("{:?}", output);
    assert!(prog.iter().zip(output).all(|(a,b)| a == b));
}

#[test]
fn day9_1_example2() {
    let program = vec![1102,34915192,34915192,7,4,7,99,0];
    let mut computer =  intcode::Intcode::new(program, vec![1 as i64]);
    let result = computer.run();
    let output = match result {
        Ok(_value) => &(computer.outputs),
        Err(err)  => if err.to_string() == "Halted" {
                       &(computer.outputs)
                    } else {
                        panic!("No valid output");
                    }   
    };
    println!("{:?}", output);
    // Require 16 digit number
    let outstr = output[0].to_string();
    assert_eq!(16, outstr.len());
}

#[test] 
fn day9_1_example3() {
    let program = vec![104,1125899906842624,99];
    let mut computer =  intcode::Intcode::new(program, vec![1 as i64]);
    let result = computer.run();
    let output = match result {
        Ok(_value) => &(computer.outputs),
        Err(err)  => if err.to_string() == "Halted" {
                       &(computer.outputs)
                    } else {
                        panic!("No valid output");
                    }   
    };
    println!("{:?}", output);
    assert_eq!(output[0], 1125899906842624);
}
