use itertools::Itertools;
use std::collections::HashMap;
use std::fs::File;
use std::io::{ BufReader, BufRead };

fn get_map(map: &Vec<String>) -> HashMap<&str, &str> {
    // turn data into map of orbiter to orbitee. 
    let mut orbits = HashMap::new();
    for n in 0..map.len() {
        if let Some((orbitee, orbiter)) = map[n].split(")").collect_tuple(){
            orbits.insert(orbiter, orbitee);
        }
    }
    orbits
}

fn get_path(map: &HashMap<&str, &str>, from: &str) -> Vec<String> {
    let mut a_path = vec![];
    let mut nxt1 = from;
    while let Some(nxt2) = map.get(nxt1) {
        a_path.push(nxt2.to_string());
        nxt1 = nxt2;
    }
    a_path
}

fn get_weight(map: &HashMap<&str, &str>) -> i32 {
    // Orbits holds the body each body moves around
    // To arrive at the count:
    //    for every node:
    //        walk the parents until COM is found. This is the bodies weight.
    //        Add all weights     
    let mut total: i32 = 0;
    for orb in map.keys() {
        let path = get_path(map, orb);
        total += path.len() as i32;
    }
    total
}

fn path_between(map: &HashMap<&str, &str>, a: &str, b: &str) -> i32 {
    let path_a: Vec<_> = get_path(map, a);
    let path_b: Vec<_> = get_path(map, b);
    println!("{}: {:?}", a, path_a);
    println!("{}: {:?}", b, path_b);
    let a_len = path_a.len();
    let b_len = path_b.len();
    let ab_same_len = path_a.iter().rev().zip(path_b.iter().rev()).take_while(|(a, b)| a == b).count();
    let res = a_len + b_len - 2 * ab_same_len;
    println!("{} + {} - 2 * {} = {}", a_len, b_len, ab_same_len, res);
    res as i32
}

fn main() {
    // Turn file content into vector of strings: "orbitee}orbiter"
    let lll = BufReader::new(File::open("day6_input.txt").expect("Cannot read inputs file"))
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<_>>();
    let map = get_map(&lll);
    let  res = get_weight(&map);
    println!("Total weight: {}", res);
    let res2 = path_between(&map, "YOU", "SAN");
    println!("Path to SAN: {}", res2);
}

#[test]
fn small_map() {
    let mapstr: Vec<String> =  
        vec!["COM)B".to_string(),
        "B)C".to_string(),
        "C)D".to_string(),
        "D)E".to_string(),
        "E)F".to_string(),
        "B)G".to_string(),
        "G)H".to_string(),
        "D)I".to_string(),
        "E)J".to_string(),
        "J)K".to_string(),
        "K)L".to_string()];
    let map = get_map(&mapstr);
    let res = get_weight(&map);
    assert_eq!(res, 42);
}

#[test]
fn small_path() {
    let mapstr: Vec<String> =  
        vec!["COM)B".to_string(),
        "B)C".to_string(),
        "C)D".to_string(),
        "D)E".to_string(),
        "E)F".to_string(),
        "B)G".to_string(),
        "G)H".to_string(),
        "D)I".to_string(),
        "E)J".to_string(),
        "J)K".to_string(),
        "K)L".to_string(),
        "K)YOU".to_string(),
        "I)SAN".to_string()];
    let map = get_map(&mapstr);
    let res = path_between(&map, "SAN", "YOU");
    assert_eq!(res, 4);
}