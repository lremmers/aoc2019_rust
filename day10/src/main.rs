use std::fs;
use std::io::{BufRead, BufReader};
use std::cmp;
use std::f64::consts::PI;

fn main() {
    let input = fs::File::open("day10_input.txt").unwrap();
    let grid = get_grid(input);
    //println!("{:?}", grid);
    let loc1 = get_best_location(&grid);  
    let Location { x, y, obs } = loc1;
    println!("@ ({},{}): {}", x, y, obs);    // @ (20,18): 280

    if let Some(zap200) = loc1.get_zapped_at(200, grid) {
        println!("nr. 200 zapped @ ({},{})", zap200.x, zap200.y);
    } else {
        println!("No 200th location found");
    }
}

// Read a grid definition from a file
fn get_grid(input:fs::File) -> Vec<Vec<bool>> {
    let reader = BufReader::new(input);
    let mut grid = vec!{};
    for line in reader.lines().map(|l| l.unwrap()) {
        let gridline : Vec<_> = line.clone().chars().map(|c| c == '#').collect();
        grid.push(gridline);
    }
    grid
}

// Get the location with the most observables
fn get_best_location(grid: &Vec<Vec<bool>>) -> Location {
    let mut best_location = Location {x: 0, y: 0, obs: 0 };
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if grid[y][x] {
                let mut loc = Location::new(x, y);
                let observed = loc.get_observables(&grid);
                loc.obs = observed.len() as i32;
                if loc.obs > best_location.obs {
                    best_location = loc;
                }
            }
        }
    }
    best_location
}

#[derive(PartialEq, Debug, Clone, Copy)]
struct Location {
    x: usize,   // position in the grid: col number 
    y: usize,   // position in the grid: row number
    obs: i32,   // number of observable other positions
}

impl Location {
    // Make a new location. No observables yet.
    fn new(x: usize, y: usize) -> Location {
        Location {x, y, obs: 0 }
    }

    // Determine the observable locations from this location, in any order
    // Also sets self.obs to the number of observables
    fn get_observables(&self, grid : &Vec<Vec<bool>>) -> Vec<Self> {
        let mut observed : Vec<Self> = vec![];
        if !grid[self.y][self.x] {
            return observed;
        };
        for y in 0..grid.len() {
            for x in 0..grid[y].len() {
                if self.is_observable(grid, x, y) {
                    observed.push(Location::new(x, y));
                    //println!("({},{} -> ({},{}: {})", self.x, self.y, x, y, self.obs);
                }
            }
        }       
        observed
    }

    // Get the nth zapped location, zapping from this location
    // n counts 1-based!
    fn get_zapped_at(&self, n: usize, mut grid: Vec<Vec<bool>>) -> Option<Self> {
        let n = n-1;        // 0-based
        // if the grid is too small, return None
        let num = grid.iter()
                      .flatten()
                      .count();
        //println!("Grid size: {}", num);
        if num < n { return  None; }

        // if there are not enough locations in the grid, return None
        let num = grid.iter()
                      .flatten()
                      .filter(|b| **b)
                      .count();
        //println!("Locations in grid: {}", num);
        if num < n { return None; }

        // Each tour, all observables will be zapped.
        // After zapping the observables, the process repeats with another tour
        let mut zapped_away = 0;
        let mut n2 = n;
        let mut zappables: Vec<Self>; 
        loop {
            zappables = self.get_observables(&grid);
            //println!("---------------------");
            //for z in zappables.iter() {
            //    println!("({},{})",z.x, z.y);
            //}
            //let num = grid.iter()
            //    .flatten()
            //    .filter(|b| **b)
            //    .count();
            let n_now = zappables.len();
            //println!("locations: {} observable: {}, need {}", num, n_now, n2);
            if n_now == 0 {
                return None;        // No zappables left to return
            }
            if zapped_away + n_now > n {
                // The required zappable is in this tour.
                let zap_n = self.get_zap_sequence(&zappables, n2);
                return Some(*zap_n);
            }
            // The required location is not zapped in the current round
            for zap in zappables {
                grid[zap.y][zap.x] = false;
            }
            zapped_away += n_now;
            n2 -=  n_now;

        }
    }

    // Determine the nth zapped locations from this location, in 1 rotation
    // zap_raw has the observables for this tour, in a 'random'order.
    fn get_zap_sequence<'a>(&self,  zap_raw: &'a Vec<Self>, n:usize) -> &'a Self {
        assert!(n < zap_raw.len());
        let mut angles: Vec<_> = zap_raw.iter()
                                    .map(|other| self.get_angle(other))
                                    .enumerate()
                                    .collect();
        angles.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        //for a in angles.iter() {
        //    println!("{:3} {:.3} ({:3},{:3})",a.0, a.1, zap_raw[a.0].x, zap_raw[a.0].y );
        //}
        &zap_raw[angles[n].0]
    }

    // return the angle in radians / PI, running from 0.0 to 2.0
    // for a Location, being observed from this location
    fn get_angle(&self, other: &Self) -> f64 {
        let x = other.x as f64 - self.x as f64;
        let y = other.y as f64 - self.y as f64;
        -x.atan2(y)/PI + 1.0
    }
    
    // See if a location is observable from this location
    fn is_observable(&self, grid: &Vec<Vec<bool>>, x: usize, y: usize) -> bool {
        if !grid[y][x] { return false; }                    // A non position is not observable

        let (x0, y0) = (self.x, self.y);                    // Shorthand
        if x == x0 && y == y0 { return false; }             // self is not observable

        let (xmin, xmax) = (cmp::min(x0, x), cmp::max(x0, x));
        let (ymin, ymax) = (cmp::min(y0, y), cmp::max(y0, y));

        if x0 == x {
            return !((ymin+1..ymax).any(|y|grid[y][x0]));
        }
        if y0 == y {
            return !((xmin+1..xmax).any(|x|grid[y0][x]));
        }

        // Problem here: we want to step positive in x from min to max
        // In the mean time, the y must increment or decrement accordingly
        
        let gcd = gr_cm_dv(xmax-xmin, ymax-ymin);                               // unsigned gcd
        let (mut dx, mut dy) = (x as i32 - x0 as i32, y as i32 - y0 as i32);    // signed range
        let (mut xx, mut yy) = (x0 as i32, y0 as i32);
        if dx < 0 {
            xx  = x as i32;
            dx = -dx;
            yy = y as i32;
            dy = -dy;
        }
        let (stepx, stepy) = (dx/gcd, dy/gcd);
        xx = xx + stepx;
        yy = yy + stepy;
        while xx < xmax as i32 {
            if grid[yy as usize][xx as usize] {
                return false;
            } else {
                xx += stepx;
                yy += stepy;
            }
        }
        true
    }
}

// greatest common dividor for a and b
fn gr_cm_dv(a: usize, b: usize) -> i32 {
    let mut a = a;
    let mut b = b;
    while  b != 0 as usize {
        let t = b;
        b = a % b;
        a = t;
    }
    a as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        println!("Running test 1");
        let input = fs::File::open("day10_example1.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);  
        assert_eq!(best_location, Location { x:3, y:4, obs:8 });
    }

    #[test]
    fn test_example2() {
        println!("Running test 2");
        let input = fs::File::open("day10_example2.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);  
        assert_eq!(best_location, Location { x:5, y:8, obs:33 });
    }

    #[test]
    fn test_example3() {
        println!("Running test 3");
        let input = fs::File::open("day10_example3.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);  
        assert_eq!(best_location, Location { x:1, y:2, obs:35 });
    }

    #[test]
    fn test_example4() {
        println!("Running test 4");
        let input = fs::File::open("day10_example4.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);  
        assert_eq!(best_location, Location { x:6, y:3, obs:41 });
    }

    #[test]
    fn test_example5() {
        println!("Running test 5");
        let input = fs::File::open("day10_example5.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);  
        assert_eq!(best_location, Location { x:11, y:13, obs:210 });
    }

    #[test]
    fn test_angles1(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new(10,   9)), 0.0 );
    }
    
    #[test]
    fn test_angles2(){
            let a = Location::new(10, 10,);
            assert_eq!(a.get_angle(&Location::new(11,   9)), 0.25);
    }
    
    #[test]
    fn test_angles3(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new(11,  10)), 0.5);
    }
    #[test]
    fn test_angles4(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new(11,  11)), 0.75);
    }
    #[test]
    fn test_angles5(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new(10,  11)), 1.0);
    }
    #[test]
    fn test_angles6(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new( 9,  11)), 1.25);
    }
    #[test]
    fn test_angles7(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new( 9,  10)), 1.50);
    }
    #[test]
    fn test_angles8(){
        let a = Location::new(10, 10,);
        assert_eq!(a.get_angle(&Location::new( 9,   9)), 1.75);
    }

    #[test]
    fn test_example2nd() {
        println!("Running test 2nd half");
        let input = fs::File::open("day10_example5.txt").unwrap();
        let grid = get_grid(input);
        let best_location = get_best_location(&grid);
        let Location {x, y, obs} =  best_location;
        println!("@ ({},{}): {}", x, y, obs);

        let loc = best_location.get_zapped_at(1, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (11, 12));

        let loc = best_location.get_zapped_at(2, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (12, 1));

        let loc = best_location.get_zapped_at(3, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (12, 2));

        let loc = best_location.get_zapped_at(10, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (12, 8));

        let loc = best_location.get_zapped_at(20, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (16, 0));

        let loc = best_location.get_zapped_at(50, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (16, 9));

        let loc = best_location.get_zapped_at(100, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (10, 16));

        let loc = best_location.get_zapped_at(199, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), ( 9, 6));

        let loc = best_location.get_zapped_at(200, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), ( 8, 2));

        let loc = best_location.get_zapped_at(201, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (10, 9));

        let loc = best_location.get_zapped_at(299, grid.clone()).unwrap();
        assert_eq!((loc.x, loc.y), (11, 1));
    }
}