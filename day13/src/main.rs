use intcode;
use std::fs;

fn main() {
    let input = fs::read_to_string("day13_input.txt").unwrap();
    let program: Vec<i64> = input
                    .trim()
                    .split(',')
                    .enumerate()
                    .map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();
    let mut computer = intcode::Intcode::new(program.clone(), vec![]);
    let mut output: Vec<i64> = vec![];
    let mut halting = false;
    let res = computer.run();
    match res {
        Ok(val) => {
            output = computer.outputs;
            halting = val;
        },
        Err(e) => println!("{}",e),
    }
    println!("Output: {:?}\n{}, Halting: {}", output, output.len(), halting);
    let blocks = output.iter().skip(2).step_by(3).filter(|n| **n == 2 ).count();
    println!("{}  blocks.", blocks);
    let mut ball = (0, 0,0);
    let mut bat = (0, 0,0);
    for i in (0..output.len()).step_by(3) {
        if output[i+2] == 3 {
            bat = (i, output[i], output[i+1]);
        }
        if output[i+2] == 4 {
            ball = (i, output[i], output[i+1]);
        }
    }
    println!("Bat at {}:({},{}), bat at {}:({},{})", bat.0, bat.1, bat.2, ball.0, ball.1, ball.2);

    // Day 2
    println!("Day 2");
    let mut computer = intcode::Intcode::new(program.clone(), vec![]);
    computer.poke(0, 2);
    let mut output: &[i64];
    let mut outcursor = 0;
    let mut ball = (0,0);
    let mut bat = (0,0);
    let mut score = 0;
    let mut halting = false;
    while !halting {
        let res = computer.run();
        match res {
            Ok(val) => {
                output = &computer.outputs[outcursor..];
                if output.len() < 40 {
                    //println!("{:?}", output);
                }
                halting = val;
            },
            Err(e) => {
                println!("{}",e);
                return;
            },
        }
        for i in (0..output.len()).step_by(3) {
            if output[i+2] == 3 {
                bat = (output[i], output[i+1]);
            }
            if output[i+2] == 4 {
                ball = (output[i], output[i+1]);
            }
            if output[i] == -1 && output[i+1] == 0 {
                score = output[i+2]
            }
        }
        let newoutputlen = output.len();
        //print!("{} outputs, Ball at ({},{}), bat at ({},{}), score: {}", newoutputlen, ball.0, ball.1, bat.0, bat.1, score);
        outcursor = newoutputlen;
        let bal2bat = ball.0.cmp(&bat.0);
        {
            computer.push_in(bal2bat as i64);
        }
        //println!(", move {}", bal2bat as i64);
    }
    println!("\nHalted with score: {}", score)
}