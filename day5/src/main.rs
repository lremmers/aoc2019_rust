use intcode;

use std::fs;

fn main() {
	let input = fs::read_to_string("day5_input.txt").unwrap();
	let list: Vec<i64> = input
					.trim()
					.split(',')
					.enumerate()
					.map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
					.collect();

	// Day 1
	println!("\nDay1:");
	let mut computer = intcode::Intcode::new(list.clone(), vec![1]);
	let _result = computer.run().unwrap();
	println!("Outputs: {:?}", computer.outputs);
	//println!("Result for input [1] is: {}", result); 

	//Day2
	println!("\nDay2:");
	let mut computer = intcode::Intcode::new(list.clone(), vec![5]);
	let _result = computer.run().unwrap();
	println!("Outputs: {:?}", computer.outputs);
	//println!("Result for input [5] is: {}", result); 
}

#[test]
fn test1_1() {
	let mut computer = intcode::Intcode::new(vec![3,9,8,9,10,9,4,9,99,-1,8], vec![8]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}

#[test]
fn test1_2() {
	let mut computer = intcode::Intcode::new(vec![3,9,8,9,10,9,4,9,99,-1,8], vec![7]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
}

#[test]
fn test2_1() {
	let mut computer = intcode::Intcode::new(vec![3,9,7,9,10,9,4,9,99,-1,8], vec![7]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}

#[test]
fn test2_2() {
	let mut computer = intcode::Intcode::new(vec![3,9,7,9,10,9,4,9,99,-1,8], vec![9]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,9,7,9,10,9,4,9,99,-1,8], vec![8]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
}

#[test]
fn test3_1() {
	let mut computer = intcode::Intcode::new(vec![3,3,1108,-1,8,3,4,3,99], vec![8]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}

#[test]
fn test3_2() {
	let mut computer = intcode::Intcode::new(vec![3,3,1108,-1,8,3,4,3,99], vec![9]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
}

#[test]
fn test4_1() {
	let mut computer = intcode::Intcode::new(vec![3,3,1107,-1,8,3,4,3,99], vec![7]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}

#[test]
fn test4_2() {
	let mut computer = intcode::Intcode::new(vec![3,3,1107,-1,8,3,4,3,99], vec![9]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,3,1107,-1,8,3,4,3,99], vec![8]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
}

#[test]
fn test5() {
	let mut computer = intcode::Intcode::new(vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], vec![0]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], vec![1]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], vec![-10]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}

#[test]
fn test6() {
	let mut computer = intcode::Intcode::new(vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1], vec![0]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![0], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1], vec![1]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
	let mut computer = intcode::Intcode::new(vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1], vec![-10]);
	let _result = computer.run().unwrap();
	assert_eq!(vec![1], computer.outputs);
}
