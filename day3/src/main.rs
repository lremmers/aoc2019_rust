use std::cmp::{min, max};
use std::fs;

#[derive(Debug, PartialEq)]
enum Dir {
    V,
    H,
}

#[derive(Debug, PartialEq)]
struct Run(i32, i32);

#[derive(Debug, PartialEq)]
struct Segment {
    dir: Dir, // Either Horizontal or Vertical
    lvl: i32, // the cordinate that does not change for the segment
    run: Run, // the segment runs from .0 to .1
}

#[derive(Debug)]
struct Wire {
    dirs: Vec<String>,      // directions to define the wire
    segments: Vec<Segment>, // list of segments making up the wire
}

fn main() {
    let input = fs::read_to_string("day3_input.txt").unwrap();
    let wire_inputs: Vec<&str> = input.lines().collect();

    let distance = get_distance(&wire_inputs);
    if let Some(d) = distance {
        println!("Distance: {}", d);
    } else {
        println!("No collision");
    }
    if let Some(d) = get_steps(&wire_inputs){ 
        println!("Steps: {}", d);
    }
}

fn get_distance(inp: &Vec<&str>) -> Option<i32> {
    let mut wires: Vec<Wire> = vec![];
    for w in inp {
        wires.push(Wire::new(w));
    }
    //println!("{:?}", wires);
    wires[0].repair_distance(&wires[1])
}

fn get_steps(inp: &Vec<&str>) -> Option<i32> {
    let mut wires: Vec<Wire> = vec![];
    for w in inp {
        wires.push(Wire::new(w));
    }
    //println!("{:?}", wires);
    wires[0].repair_steps(&wires[1])
}

#[test]
fn test_example1() {
    let inputs = vec!["R8,U5,L5,D3", "U7,R6,D4,L4"];
    assert_eq!(Some(6), get_distance(&inputs));
    assert_eq!(Some(30), get_steps(&inputs));
}

#[test]
fn test_example2() {
    let inputs = vec![
        "R75,D30,R83,U83,L12,D49,R71,U7,L72",
        "U62,R66,U55,R34,D71,R55,D58,R83",
    ];
    assert_eq!(Some(159), get_distance(&inputs));
    assert_eq!(Some(610), get_steps(&inputs));
}

#[test]
fn test_example3() {
    let inputs = vec![
        "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
        "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
    ];
    assert_eq!(Some(135), get_distance(&inputs));
    assert_eq!(Some(410), get_steps(&inputs));
}

struct Point {
    x: i32,
    y: i32,
}

impl Wire {
    fn new(input: &str) -> Wire {
        let dirs: Vec<String> = input.split(',').map(|s| s.to_string()).collect();
        let mut head = Point { x: 0, y: 0 };
        let segments: Vec<Segment> = dirs
            .iter()
            .map(|s| Segment::new(s, &mut head).unwrap())
            .collect();
        Wire { dirs, segments }
    }

    fn repair_distance(&self, other: &Wire) -> Option<i32> {
        let mut result = i32::MAX;
        for sgm1 in &self.segments {
            for sgm2 in &other.segments {
                if let Some(d) = sgm1.collision_distance(sgm2) {
                    if result > 0 {
                        result = min(result, d);
                        //println!("{:?}/{:?}: {}", sgm1, sgm2, result)
                    }
                }
            }
        }
        if result == i32::MAX {
            None
        } else {
            Some(result)
        }
    }

    fn repair_steps(&self, other: &Wire) -> Option<i32> {
        let mut result = i32::MAX;
        let mut sgm1_steps = 0;
        for sgm1 in &self.segments {
            let mut sgm2_steps = 0;
            for sgm2 in &other.segments {
                if let Some(p) = sgm1.collides(sgm2){
                    let dst1 = sgm1_steps + sgm1.lento(&p);
                    let dst2 = sgm2_steps + sgm2.lento(&p);
                    if dst1 + dst2 > 0 {
                        result = min(result, dst1 + dst2);
                    }
                }
                sgm2_steps += sgm2.len();
            }
            sgm1_steps += sgm1.len();
        }
        if result == i32::MAX {
            None
        } else {
            Some(result)
        }
    }
}

impl Segment {
    fn new(dir: &str, head: &mut Point) -> Option<Segment> {
        let (direction, distance) = dir.split_at(1);
        let distance: i32 = distance.parse().unwrap();
        let segm = match direction {
            "U" => {
                let sgm = Segment {
                    dir: Dir::V,
                    lvl: head.x,
                    run: Run(head.y, head.y + distance),
                };
                head.y += distance;
                Some(sgm)
            }
            "D" => {
                let sgm = Segment {
                    dir: Dir::V,
                    lvl: head.x,
                    run: Run(head.y, head.y - distance),
                };
                head.y -= distance;
                Some(sgm)
            }
            "L" => {
                let sgm = Segment {
                    dir: Dir::H,
                    lvl: head.y,
                    run: Run(head.x, head.x - distance),
                };
                head.x -= distance;
                Some(sgm)
            }
            "R" => {
                let sgm = Segment {
                    dir: Dir::H,
                    lvl: head.y,
                    run: Run(head.x, head.x + distance),
                };
                head.x += distance;
                Some(sgm)
            }
            _ => {
                println!("Unknown direction '{}'", direction);
                None
            }
        };
        segm
    }

    fn len(&self) -> i32 {
        (self.run.1 - self.run.0).abs()
    }

    fn collides(&self, other: &Segment) -> Option<Point> {
        let (a0, a1) = (min(self.run.0,  self.run.1),  max(self.run.0,  self.run.1));
        let (b0, b1) = (min(other.run.0, other.run.1), max(other.run.0, other.run.1));
        if self.dir != other.dir {
            // crossing segments?
            if self.lvl >= b0
                && self.lvl <= b1
                && other.lvl >= a0
                && other.lvl <= a1
            {
                if self.dir == Dir::H {
                    Some(Point{x: other.lvl, y: self.lvl})
                } else {
                    Some(Point{x: self.lvl, y: other.lvl})
                }
            } else {
                // no collision
                None
            }
        } else {
            // No collission if in the same direction
            // That is not entirely correct
            None
        }
    }

    fn lento(&self, p: &Point) -> i32 {
        // point P should be on the segment
        if self.dir == Dir::H && self.lvl == p.y {
            (p.x - self.run.0).abs()
        } else if self.dir == Dir::V && self.lvl == p.x {
            (p.y - self.run.0).abs()
        } else {
            panic!("lento: Point p is not on the segment")
        }
    }

    fn collision_distance(&self, other: &Segment) -> Option<i32> {
        let (a0, a1) = (min(self.run.0,  self.run.1),  max(self.run.0,  self.run.1));
        let (b0, b1) = (min(other.run.0, other.run.1), max(other.run.0, other.run.1));
        let result = if self.dir == other.dir && self.lvl == other.lvl {
            // these same direction segments might be colliding
            if a1 >= b0 {
                if a1 * b0 <= 0 {
                    Some(self.lvl)
                } else {
                    println!("Same direction: {:?}, {:?}", self, other);
                    Some(self.lvl.abs() + min(a1.abs(), b0.abs()))
                }
            } else if b1 >= a0 {
                if b1 * a0 < 0 {
                    Some(self.lvl.abs())
                } else {
                    Some(self.lvl.abs() + min(b1.abs(), a0.abs()))
                }
            } else {
                // no collision
                None
            }
        } else {
            if self.dir != other.dir {
                // crossing segments?
                if self.lvl >= b0
                    && self.lvl <= b1
                    && other.lvl >= a0
                    && other.lvl <= a1
                {
                    Some(self.lvl.abs() + other.lvl.abs())
                } else {
                    // no collision
                    None
                }
            } else {
                None
            }
        };
        if let Some(0) = result {
            None
        } else {
            result
        }
    }
}

