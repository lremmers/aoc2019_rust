// Local modules, declared in their own file <mdulename>.rs
pub mod keyset;
pub mod maze;

use std::fs;
use std::collections::{HashSet, HashMap, VecDeque};
use std::time::{Instant};
use clap;

use keyset::KeySet;
use maze::{ Maze, DirT, Location, Edge};

fn main() {
    let matches = clap::App::new("Maze solver")
                    .version("0.0")
                    .author("Len Remmerswaal")
                    .about("Aoc2019 Day18 Part 1")
                    .arg(clap::Arg::with_name("input")
                            .help("Holds maze definition")
                            .index(1))
                    .arg(clap::Arg::with_name("color")
                            .help("print a ANSI coloured map i.o. a char map")
                            .short("c")
                            .long("color")
                            .takes_value(false))
                    .arg(clap::Arg::with_name("reduce_map")
                            .help("Find a reduced map")
                            .short("r")
                            .long("reduce")
                            .takes_value(false))
                    .arg(clap::Arg::with_name("edges")
                            .help("Print all edges (lots of output!)")
                            .short("e")
                            .long("edges")
                            .takes_value(false))
                    .arg(clap::Arg::with_name("part2")
                            .help("Perform part 2 of the puzzle")
                            .short("2")
                            .takes_value(false))
            .get_matches();

    let fname = matches.value_of("input").unwrap_or("day18_input.txt");
    let mazestr = fs::read_to_string(fname).expect(&format!("Cannot read file {}", fname));

    if matches.is_present("color") {
        maze::print_maze_str_coloured(&mazestr);
    } else {
        maze::print_maze_str(&mazestr);
    }

    let now = Instant::now();
    let mut maze = Maze::new(&mazestr);
    println!("Create maze in {}s", now.elapsed().as_secs_f32());
    println!("{}", maze);
    let num_starts = maze.starts_len();

    if matches.is_present("reduce_map"){
        let now = Instant::now();
        let cursize = maze.len();
        maze.reduce();
        if maze.len() != cursize {
            maze.paint( matches.is_present("color"));
        }
        println!("Reduce maze in {}s", now.elapsed().as_secs_f32());
    }

    let prnt = matches.is_present("edges");
    let now = Instant::now();
    let mut network = Network::new(&maze, prnt);
    println!("Create network in {}s", now.elapsed().as_secs_f32());

    if num_starts == 1 {
        let now = Instant::now();
        let result = network.get_all_keys();
        println!("\nAll keys in {} steps", result);
        println!("Solve maze in {}s", now.elapsed().as_secs_f32());
    }

    if matches.is_present("part2"){
        let mazes = maze.split_into_4();
        for m in mazes.iter() {
            m.paint(true);
            println!();
        }

        let now = Instant::now();
        let mut network = Network::new4(&mazes);
        println!("Create 4 networks in {}s", now.elapsed().as_secs_f32());

        let now = Instant::now();
        let result = network.get_all_keys_4();
        println!("\nAll keys in {} steps", result);
        println!("Solve maze in {}s", now.elapsed().as_secs_f32());
    }
}

/// Used by Network::construct_edge() to find a path in the maze from key to key
struct QueData {
    loc: Location,
    needed: KeySet,
    dist: usize,
    path: Vec<Location>,
}

impl QueData {
    fn new(loc: Location, needed: KeySet, dist: usize, path: Vec<Location>) -> Self {
            Self { loc, needed, dist, path}
    }
}

/// Used by the search routine through the network to find all keys
#[derive(Clone, Debug)]
struct SearchNode {
        key: char,          // location of the node to assess
        dist: usize,        // distance up to this node using this path 
        hold: KeySet,       // Keys being carried: should always correspond to path
        path: Vec<char>,    // Path of picked up keys   
        prevrnd: usize,     // Round where this node was pushed into the queue
}

impl SearchNode {
    fn new(key: char, dist: usize, path: Vec<char>, prevrnd: usize) -> Self {
        let mut hold = KeySet::new();
        for k in &path {
            hold.set(*k);
        }
        SearchNode { key, dist, hold, path, prevrnd }
    }
}

/// Used by the search routine through the network to find all keys
#[derive(Clone, Debug)]
struct SearchNode4 {
        loc: [char;4],      // location of the node to assess
        dist: usize,        // distance up to this node using this path 
        hold: KeySet,       // Keys being carried: should always correspond to path
        path: Vec<char>,    // Path of picked up keys   
        prevrnd: usize,     // Round where this node was pushed into the queue
}

impl SearchNode4 {
    fn new(loc: [char;4], dist: usize, path: Vec<char>, prevrnd: usize) -> Self {
        let mut hold = KeySet::new();
        for k in &path {
            hold.set(*k);
        }
        SearchNode4 { loc, dist, hold, path, prevrnd }
    }
}


/// Definition of a network
struct Network {
    /// size of the network, including '@'
    /// Keyset holding all possible keys in the network
    all_keys: KeySet,

    /// Map of keysset, held when each node gets visited, mapping to the distance when visited with that keyset
    /// In parallel, store the round where this happened
    dists: HashMap<(char, KeySet), usize>,
    rounds: HashMap<(char, KeySet), usize>,

    /// List of all edges, from key1 to key2 
    /// There are size edge in the Map
    edges: HashMap<(char, char), Edge>,
}

impl Network {

    /// Create the network from the maze data
    /// Each edge in the base network is from a key loc to a key loc (including '@')
    /// Each edge is weighted, and the doors and keys found along the way are recorded
    fn new(maze: &Maze, prnt: bool) -> Network {    

        let size = maze.keynames().len() + 1;  // including @
        // Create a completely empty network
        let mut nw = Network {
                               all_keys: KeySet::from_str("@"),
                               dists: HashMap::with_capacity(size), 
                               rounds: HashMap::with_capacity(size),
                               edges: HashMap::with_capacity(size*size) 
        }; 

        for key in maze.keynames().iter() {
            nw.all_keys.set(*key);
            nw.dists.insert(('@', KeySet::from_str("@")), 0);
            nw.rounds.insert(('@', KeySet::from_str("@")), 0);
        }
    
        for (n1, key1) in maze.keynames().iter().enumerate() {
            let &loc1 = maze.location_from_char(*key1).unwrap();
            for (_n2, key2) in maze.keynames().iter().enumerate().skip(n1) {
                let &loc2 =  maze.location_from_char(*key2).unwrap();
                let edges = Network::construct_edge(maze, loc1, loc2);
                if !edges.is_empty() {
                    //println!("{}->{}: {}: N[{}]", key1, key2, edges[0].dist, edges[0].needed);
                    nw.edges.insert((*key1, *key2), edges[0].clone());
                    nw.edges.insert((*key2, *key1), edges[0].clone());
                    if prnt {
                        maze.paint_edge_in_maze(*key1, *key2, edges[0].path.clone());
                    }
                }
            }
        }
        nw
    }

    /// While contructing the network from the maze:
    /// find all paths from loc1 to loc2, collecting distance, passed doors and passed keys as you go
    /// Of different paths with identical Door sets, the shortest is returned
    /// Difffent paths with different Door sets are all returned.
    /// If prnt is true, the edge is printed in the maze
    fn construct_edge(maze: &Maze, loc1: Location, loc2: Location) -> Vec<Edge> {
        if loc1 == loc2 {
            return vec![];
        }
        let mut queue: VecDeque<QueData> = VecDeque::with_capacity(maze.len()); // locations to handle
        let mut visited: HashSet<Location> = HashSet::with_capacity(maze.len()); // locations visited, with distance from start 
        let mut edges : Vec<Edge> = Vec::with_capacity(5);

        //println!("Map: {:?}", maze.map);
        queue.push_back( QueData::new(loc1, KeySet::new(), 0, vec![loc1])  );   // start with  searching from start
        while let Some(base) = queue.pop_front() {    // base is the location being handled

            visited.insert(base.loc);               // This location is now handled
            //print!(" At {}", base.loc);
            for dir in DirT::iterator() {
                let testloc = match dir {
                    DirT::North => Location::new(base.loc.x , base.loc.y-1),
                    DirT::South => Location::new(base.loc.x , base.loc.y+1),
                    DirT::West => Location::new(base.loc.x-1, base.loc.y),
                    DirT::East => Location::new(base.loc.x+1, base.loc.y),
                };
                //print!(" looking at {}", testloc);
                
                if !maze.has(testloc){
                    //println!(" no path");
                    continue;           // Not part of the maze
                }
                
                if visited.contains(&testloc) {
                    //println!(" been there");
                    continue;           //  Been there
                }

                //println!(" prospect");
                let mut needed = base.needed;      // Keys for doors found so far
                let distance   = base.dist + 1;    // Distance from loc1     
                if let Some(&door) = maze.door_from_loc(testloc) {      
                    //There is a door here. Register its key on the needed set
                    needed.set(Maze::door2key(door));
                }
                if testloc == loc2 {
                    // Is there a path with the same keyset and a larger path?
                    let similar: Vec<_> = edges.iter().enumerate()
                                       .filter(|(_, e)| e.needed == needed && e.dist > distance )
                                       .map(|(n,_)| n)
                                       .collect();
                    // Then remove them
                    for n in similar{
                        edges.remove(n);
                    }
                    // Is there a path with the same keyset and a shorter path?
                    let similar: Vec<_> = edges.iter().enumerate()
                                       .filter(|(_, e)| e.needed == needed && e.dist < distance )
                                       .map(|(n,_)| n)
                                       .collect();
                    if similar.is_empty() {
                        // No:  
                        let mut path = base.path.clone();
                        path.push(testloc);
                        let edge = Edge{ 
                                from: *maze.key_from_loc(loc1).unwrap(),
                                to: *maze.key_from_loc(loc2).unwrap(),
                                dist: distance,
                                needed,
                                path,
                                maze_id: 0,
                        };
                        edges.push(edge);
                    }
                    continue;                   // This path is complete
                }
                if let Some(&key) = maze.key_from_loc(testloc) {      
                    //There is a key here, and its not the final key at loc2. Register this key in the needed set.
                    // This means you cannot pass it when you don't hold it
                    needed.set(key);
                }
                let mut path = base.path.clone();
                path.push(testloc);
                queue.push_back(QueData{ loc: testloc, needed, dist: distance, path });
            }
        }
        edges
    }

    /// # Get all keys and report the steps taken
    /// The network is at most 27 nodes large, each identified by a Key name ('@', 'a'-'z').
    /// Visiting each node you find a key with the same name.
    /// This is a fully connected non-directional graph wit weighed edges, but edges may be blocked.
    /// An edge is blocked if going over it:
    /// - you encounter another key in the middle: 
    ///   you should visit that key's node first, with no loss of distance
    /// - you encounter a door in the middle: 
    ///   you need to visit its key's node first, to unlock the door.
    /// If you have a key, there is no need to ever visit that node again: the graph is fully connected.
    /// It may be that another path, due to unlocking sequences, is shorter. 
    /// So you do need a backtracking mechanism
    /// We do a BFS search to visit all nodes. 
    /// At each node a map is recorded that maps a keyset being held to arrive at the node, to a distance. 
    /// If ever a node is visited with recorded smaller distance while the keyset for finding that shorter distance 
    /// is equal or larger then the current path's keyset: abort the path: less or equal keys found with 
    /// more effort is a dead end.
    /// Any node visited is unconditionally marked as such by holding its key.
    /// Any node is available if it is not visited, and the edge going to it is unlocked.
    /// ## Each node holds:
    /// - a map of keys held when visited to the minimum distance for each different keyset.
    /// ## Each queue position holds:
    ///    loc: the location to explore, path: route walked to get to loc, dist: distance travelled to loc. 
    /// ## Steps:
    /// 0 Put the start ('@' on layer <emtpy>) on a queue with an path '@' and distance 0
    ///   Provide each node with an empty keyset to distance map
    /// 1 Pop the next path (base) from the stack. If there are none: end
    /// 2 If the base node holds all keys (endpoint found):
    ///   Record the path, and the steps needed to traverse it, if its length is smaller then the currently stored
    ///   Then goto 1: no need to search for more keys as you have them all
    /// 3 identify all reachable non-visited keys from base node, 
    ///   i.e. the edge's held keyset intersect needed keyset = needed keyset 
    ///   If no new keys were found: go to 1.
    ///   If new keys were found
    ///      -  check if the new node holds the same keyset, and if so, has a smaller distance for that keyset
    ///      - if so: goto 1: this path is not interesting
    ///      - if not so: 
    ///           Add the new keyset/distance pair to the node's map
    ///           Add a new item to the queue with all data needed to get to that new node
    /// 4 Go back to 1

    fn get_all_keys(&mut self) -> usize {

        // The search stack: locations + distance from start + keys found at step
        let mut search_stack: VecDeque<SearchNode> = VecDeque::new();
        let mut rounds = 0;
        let mut mindist = usize::MAX;

        search_stack.push_back( SearchNode::new('@', 0, vec!['@'], 0));
        while let Some(state) = search_stack.pop_front() {
            let holding = state.hold;
            rounds += 1;
            // if rounds > 6 {
            //     panic!("Exit");
            // }

            // println!("\rRound {:6}/{:6}: At key {}, distance {:5}, have keys {} in seq: {}", 
            //             rounds, state.prevrnd, state.key,  state.dist,    holding, &state.path.iter().collect::<String>());

            if holding == self.all_keys {
                // End point found
                self.record_possible_solution(&state, &mut mindist, rounds);
                continue;
            } 

            let reachable = self.find_reachable_keys(&state);
            let new_keys = reachable.diff(&holding);
            // println!("Rchbl: {}, found: {} new: {}", reachable, holding, new_keys);
            if new_keys.is_empty() {
                continue;
            }

            // New keys reachable
            for k in new_keys.as_hashset().iter() {
                // Is the new path already trumped?
                let mut new_ks = holding;
                new_ks.set(*k);

                let new_dist = self.edges[&(state.key, *k)].dist + state.dist;
                
                let trumped = match self.dists.get(&(*k, new_ks)) {
                    Some(value) => *value <= new_dist,
                    None => false,
                };
                if !trumped {
                    // Update the node with the new best distance
                    self.dists.insert((*k, new_ks), new_dist);
                    self.rounds.insert((*k, new_ks), rounds);
                    // Insert this new key in the search queue
                    let mut new_path= state.path.clone();
                    new_path.push(*k);
                    let sn = SearchNode::new(*k, new_dist, new_path, rounds);
                    search_stack.push_back(sn);                    
                // } else {
                //     let old_dist = self.dists.get(&(*k, new_ks)).unwrap();
                //     println!("Already have {}:{} with {}<{}", *k, new_ks, *old_dist ,new_dist )
                }
            }
        }
        mindist
    }

    /// record_possiblesolution inspects the path walked and updates the mindist value if
    /// the path warrants that
    fn record_possible_solution(&mut self, state: &SearchNode, mindist: &mut usize, round: usize) {
        let wantprint = false;
        let dist = state.dist;

        if wantprint { 
             print!("{:8} {:4}?  {}", round, dist, &state.path.iter().collect::<String>());
        }
        if dist < *mindist {   
            print!("\n{:8} {:4}?! {}", round, dist, &state.path.iter().collect::<String>());
            *mindist = dist;

        } else if wantprint && dist == *mindist {
            println!(" =")
        } else if wantprint {
            println!();
        }
    }
    
    /// Find reachable keys from the current location
    /// Reachable keys are keys over edges going from the current key and having a needed keyset smaller then 
    /// the currently found keyset.
    fn find_reachable_keys(&self, state: &SearchNode) -> KeySet {
        // self.edges.iter() produces items ( (&key1, &key2), &edge )
        let mut ks = KeySet::new();
        for ((k1, k2), e) in self.edges.iter() {
            if *k1 == state.key && 
                state.hold.has_all(&e.needed) {
                    ks.set(*k2);
            }
        }
        ks    
    }

    /// Create the network from the 4 maze data
    /// Each edge in the base network is from a key loc to a key loc (including '@')
    /// Each edge is weighted, and the doors and keysfound along the way are recorded
    /// A single network is recorded , with 4 start points
    /// New4 produces a day18 part2 network
    fn new4(mazes:  &Vec<Maze>) ->  Network {

        let size = mazes.iter()
                        .map(|m| m.keynames().len() )
                        .sum::<usize>()+1;  // including a single @
        // Create a completely empty network
        let mut nw = Network {
                               all_keys: KeySet::from_str("@"),
                               dists: HashMap::with_capacity(size), 
                               rounds: HashMap::with_capacity(size),
                               edges: HashMap::with_capacity(size*size)  // max case 
                            };
        
        nw.dists.insert(('@', KeySet::from_str("@")), 0);
        nw.rounds.insert(('@', KeySet::from_str("@")), 0);
        for (n, m) in mazes.iter().enumerate() {
            for key in m.keynames().iter() {
                nw.all_keys.set(*key);
            }

            //println!("Network {}: keys: {:?}", n, m.keynames());
            for (n1, key1) in m.keynames().iter().enumerate() {
                let &loc1 = m.location_from_char(*key1).unwrap();
                //print!("From '{}' @ {}", key1, loc1);
                for (_n2, key2) in m.keynames().iter().enumerate().skip(n1) {
                    if key1 == key2 { continue;}
                    let &loc2 =  m.location_from_char(*key2).unwrap();
                    //print!(" to {} @ {}", key2, loc2);
                    let mut edges = Network::construct_edge(m, loc1, loc2);
                    if !edges.is_empty() {
                        edges[0].maze_id = n;
                        //println!("{}: {}->{}: {}: N[{}]", edges[0].maze_id, key1, key2, edges[0].dist, edges[0].needed);
                        nw.edges.insert((*key1, *key2), edges[0].clone());
                        nw.edges.insert((*key2, *key1), edges[0].clone());
                    //} else {
                    //    println!("{}: {}->{}: no edge found", n, key1, key2)
                    }
                }
            }
        }
        nw    
    }

    /// Get allkeys from the network in 4 mazes
    /// This is done the same as for the single maze case, except there is not a single position on the
    /// queue but a 4-tuple of positions.
    /// A branch is pruned like before if a new equal or larger distance is found for an already found keyset.
    fn get_all_keys_4(&mut self) ->  usize {
        // The search stack: locations + distance from start + keys found at step
        let mut search_stack: VecDeque<SearchNode4> = VecDeque::new();
        let mut rounds = 0;
        let mut mindist = usize::MAX;

        search_stack.push_back( SearchNode4::new(['@','@','@','@'], 0, vec!['@'], 0));
        while let Some(state) = search_stack.pop_front() {
            let holding = state.hold;
            rounds += 1;
            // if rounds > 6 {
            //     panic!("Exit");
            // }

            //println!("\rRound {:6} {:6} At key {:?}, distance {:5} have keys {} in seq: {}", 
            //           rounds, state.prevrnd, state.loc,  state.dist,    holding, &state.path.iter().collect::<String>());

            if holding == self.all_keys {
                // End point found
                self.record_possible_solution4(&state, &mut mindist, rounds);
                continue;
            } 

            let reachable = self.find_reachable_keys4(&state);
            let new_keys = reachable.diff(&holding);
            //println!("Rchbl: {}, found: {} new: {}", reachable, holding, new_keys);
            if new_keys.is_empty() {
                continue;
            }

            // New keys reachable
            for k in new_keys.as_hashset().iter() {
                // Is the new path already trumped?
                let mut new_ks = holding;
                new_ks.set(*k);

                let e_id = self.get_edge_data_with_key(*k).unwrap();
                let k_from = state.loc[e_id];
                let e = &self.edges[&(k_from,*k)];

                let new_dist = state.dist +  e.dist;

                let trumped = match self.dists.get(&(*k, new_ks)) {
                    Some(value) => *value <= new_dist,
                    None => false,
                };
                if !trumped {
                    //println!("Next: maze {} {} -> {} {}+{}={}", e_id, k_from, *k, state.dist, e.dist, new_dist);
                    // Update the node with the new best distance
                    self.dists.insert((*k, new_ks), new_dist);
                    self.rounds.insert((*k, new_ks), rounds);
                    // Insert this new key in the search queue
                    let mut new_path= state.path.clone();
                    new_path.push(*k);
                    let mut loc = state.loc;
                    loc[e_id] = *k;
                    let sn = SearchNode4::new(loc, new_dist, new_path, rounds);
                    search_stack.push_back(sn);                    
                }
            }
        }
        mindist
    }

    fn record_possible_solution4(&mut self, state: &SearchNode4, mindist: &mut usize, round: usize) {
        let want_print = false;
        let dist = state.dist;
        if want_print { 
             print!("{:8} {:4}?  {}", round, dist, &state.path.iter().collect::<String>());
        }
        if dist < *mindist {   
            print!("\n{:8} {:4}?! {}", round, dist, &state.path.iter().collect::<String>());
            *mindist = dist;
        } else if want_print && dist == *mindist {
            println!(" =")
        } else if want_print {
            println!();
        }
    
        
    }

    /// Find all reachable keys in all 4 mazes.
    fn find_reachable_keys4(&self, state: &SearchNode4) -> KeySet {
        let mut ks = KeySet::new();
        for ((k1, k2), e) in self.edges.iter() {
            if state.loc.iter().any(|c| *c == *k1 ) &&
            // if *k1 == state.key && 
                state.hold.has_all(&e.needed) {
                    ks.set(*k2);
            }
        }
        ks    
    }

    /// Find the maze with the edge that begins or ends at key k
    fn get_edge_data_with_key(&self, key: char) -> Option<usize> {
        for (&k, v) in self.edges.iter() {
            if k.0 == key || k.1 == key {
                return Some(v.maze_id);
            }
        }
        None
    }
}    
    

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input = "
#########
#b.A.@.a#
#########
".trim();
        println!("{}", input);
        let maze = Maze::new(&input);
        let mut network = Network::new(&maze, false);
        let result = network.get_all_keys();
        assert_eq!(result,8);   // 8
    }

    #[test]
    fn test2() {
        let input = "
########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################
".trim();
        println!("{}", input);
        let maze = Maze::new(&input);
        let mut network = Network::new(&maze, false);
        let result = network.get_all_keys();
        assert_eq!(result,86);  // 86
    }

    #[test]
    fn test3() {
        let input = "
########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################
".trim();
    println!("{}", input);
    let maze = Maze::new(&input);
    let mut network = Network::new(&maze, false);
    let result = network.get_all_keys();
assert_eq!(result,132);    // 132
    }

    #[test]
    fn test4() {
        let input = "
#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################
".trim();
        println!("{}", input);
        let maze = Maze::new(&input);
        let mut network = Network::new(&maze, false);
        let result = network.get_all_keys();
        assert_eq!(result,136);  // 136
    }

    #[test]
    fn test5() {
        let input = "
########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################"
.trim();
        println!("{}", input);
        let maze = Maze::new(&input);
        let mut network = Network::new(&maze, false);
        let result = network.get_all_keys();
        assert_eq!(result,81);  //81
    }

}   