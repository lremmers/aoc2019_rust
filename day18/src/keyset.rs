
use std::collections::HashSet;
use std::fmt;

/// A collection of keys.
/// Each key can be represented by a char: '@' and 'a'-'z'.
/// Thus there are at most 27 keys.
/// The underling storage is a single u32.
/// This makes the KeySet very small, Copy-able and Hash-able
/// Each key is present if its corresponding bit is set

#[derive(Clone, Copy, Debug, Hash, PartialEq)]
pub struct KeySet { d:u32 }

impl Eq for KeySet {}

impl KeySet {

    /// # Constructors 

    /// Return a new empty KeySet
    pub fn new() -> Self {
        Self { d:  0 }
    }

    // Return a keyset with a key for each valid key char in str
    // If no valid chars are encountered, an empty keyset is returned
    pub fn from_str(keys: &str) -> Self {
        let mut ks = KeySet::new();
        for c in keys.chars() {
            ks.set(c);
        }
        ks
    }

    /// Create a keyset with keys for each coresponding set bit in d
    /// Bits above number 27 are ignored.
    pub fn from_usize(d: usize) -> Self {
        Self { d: d  as u32 & ((1 << 28) - 1) }
    }

    /// Create a keyset from all valid key chars in hs
    /// IF no valid chars are in hs, an empty keyset is returned.
    pub fn from_set(hs: &HashSet<char> ) -> Self {
        let mut ks = KeySet::new();
        for c in hs {
            ks.set(*c);
        }
        ks
    }

    /// # In place changers (mut &self -> ())

    /// Add a single key.
    /// If key is not a valid  character, nothing happens
    pub fn set(&mut self, key: char){
        if let Ok(value) = Self::key2i(key) {
            self.d |= 1 << value;
        }
    }

    /// Remove a single key.
    /// If key is not a valid  character, nothing happens
    pub fn reset(&mut self, key: char) {
        if let Ok(value) = Self::key2i(key) {
            self.d &= !(1 << value);
        }
    }

    /// Add all keys from other to self
    pub fn add(&mut self, other: &Self) {
        self.d |= other.d & ((1<<28) - 1)
    }

    /// Remove all keys in other from self
    pub fn remove(&mut self, other: &KeySet) {
        self.d &= !(other.d & ((1<<28) - 1))
    }
        
    /// # Inspectors (ref self -> T)

    /// Return true if there are no keys in the set
    pub fn is_empty(&self) -> bool {
        self.d == 0
    }

    /// Return true if the given key is in the KeySet 
    /// if key is not a valid character, false is always returned
    pub fn has(&self, key: char) -> bool {
        match Self::key2i(key) {
            Ok(value) => (1 << value) & self.d != 0,
            Err(_) => false,
        }
    }

    /// See if all keys of other are in self
    pub fn has_all(&self, other: &Self) -> bool {
        self.d & other.d == other.d
    }

    /// Returns the number keys in the set
    pub fn len(&self) -> usize {
        (0..=27).filter(|i| 1 << i & self.d != 0 ).count()
    }

    /// Allow using keyset as an index
    pub fn as_usize(&self) -> usize {
        self.d as usize
    }

    /// Create a HashSet from a KeySet
    pub fn as_hashset(&self) -> HashSet<char> {
        (0..=27).filter(|i| (1 << i) & self.d != 0 )
                .map(|i| Self::i2key(i).unwrap() )
                .collect()
    }

    /// # Operators: returning new keyset (ref self -> Self) 
    
    /// Return a keyset with all keys common in both sets
    pub fn intersection(&self, other: &Self) -> Self {
        Self { d: self.d & other.d }      
    }

    /// Return a keyset with all keys found in any of the 2 sets 
    pub fn union(&self, other: &Self) -> Self {
        Self { d: self.d | other.d }      
    }

    /// Return a a copy of self with all keys in other removed
    pub fn diff(&self, other: &Self) -> Self {
        Self { d: self.d & !other.d }
    }

    /// # Private helper functions

    /// Return the bit index corresponding to key, if key is valid
    fn key2i(key: char) -> Result<u8, ()> {
        if key == '@' { 
            Ok(0) 
        } else {
            let keynum = key as u8 - 'a' as u8 + 1;
            if keynum >= 1 && keynum <= 27 {
                Ok(keynum)
            } else {
                Err(())
            }
        }
    }

    /// Return the key name (char) corresponding to bit index i, if it is set and valid
    fn i2key(i: u8) -> Option<char> {
        match i {
            0      => Some('@'),
            1..=27 => Some( ('a' as u8 + i-1 ) as char),
            _      => None, 
        }
    }
}

impl fmt::Display for KeySet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let set = (0..=27).filter(|i| (1 << i) & self.d != 0 )
                          .map(|i| KeySet::i2key(i).unwrap())
                          .collect::<String>();
        write!(f, "{}", set)
    }
}

