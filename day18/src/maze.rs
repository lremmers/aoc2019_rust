
use std::fmt;
use std::collections::{ HashSet, HashMap };
use crate::keyset::KeySet;

use ansi_term::{ Colour,Style };

pub fn print_maze_str(mstr: &str) {
    println!("{}",mstr);
}

pub fn print_maze_str_coloured(mstr: &str) {
    for  chr in mstr.chars() {
        match chr {
            '#'       => print!("{}", color_wall().paint(" ")),             // wall
            '.'|' '   => print!("{}", color_floor().paint(" ")),            // walkway
            'a'..='z' => print!("{}", color_key().paint(chr.to_string())),  // key
            'A'..='Z' => print!("{}", color_door().paint(chr.to_string())), // door
            '@'       => print!("{}", color_key().paint(chr.to_string())),  // start
            _         => print!("{}", chr), 	                            // anything else 
        }
    }
}

fn color_wall() -> Style {
    Colour::Black.dimmed().on(Colour::Fixed(242)).dimmed()
}

fn color_floor() -> Style {
    Colour::Black.on(Colour::White)
}

fn color_key() -> Style {
    Colour::Red.bold().on(Colour::White)
}

fn color_door() -> Style {
    Colour::Blue.bold().on(Colour::White)
}

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub struct Location {
    pub x: i32,
    pub y: i32,
}
impl Eq for Location {}

impl Location {
    pub fn new(x: i32, y:i32) -> Self {
        Location {x, y}
    }

    pub fn from_tpl( tpl: (i32, i32)) -> Self {
        Location { x: tpl.0, y: tpl.1 }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum DirT {
    West,
    East,
    North,
    South,
}

impl DirT {
    pub fn iterator() -> impl Iterator<Item = DirT> {
        [DirT::North, DirT::South, DirT::East, DirT::West].iter().copied()
    }
}

/// Attributes for an edge
#[derive(Clone, Debug)]
pub struct Edge {
    pub from: char,
    pub to: char,
    pub needed: KeySet,         // Keys needed to unlock this edge
    pub dist: usize,            // Weigth of the edge: distance to travel it.
    pub path: Vec<Location>,
    pub maze_id: usize,         // Maze index where the edge exists
}

// A Maze captures a 2D-maze in a set of locations, key-locations, door-locations and a start location
// The upper-left location is (0,0)
// If size == (x,y) then the lower-right locations is (x-1, y-1)
// The maze is surrounded by walls. The entrance is somewhere in the middle, at the location of 'key' '@'
//
// For part 2, the maze transforms into 4 mazes. Each maze gets its own 0 and its own size.
// It is up to the maze instantioator to handle the 4 mazes

#[derive(Clone)]
pub struct Maze {
    // base topology
    id: usize,                          // id of the maze in a multi-maze situation
    size: (usize, usize),               // The size of the map in (x, y) direction        
    pub map: HashSet<Location>,             // All walkable locations in the maze
    start: Location,                    // Start location
    keys: HashMap<Location, char>,      // locations of the keys, and which key it is
    doors: HashMap<Location, char>,     // locations of the doors, and which door it is
    keys2loc: HashMap<char, Location>,  // reverse mapping of keys (by index) to location
    keynames: Vec<char>,                // provides sorted iterator over all found keynames
    starts: Vec<Location>,              // vector of starts
}

impl Maze {
    pub fn new(inp: &str) -> Self {
        let id: usize = 0;
        let map = HashSet::new();
        let start = Location::new(0,0);
        let keys = HashMap::new();
        let keys2loc = HashMap::new();
        let doors = HashMap::new();
        let keynames = Vec::new();
        let starts = Vec::new();

        let mut maze = Self { id, map, start, keys, keys2loc, keynames, doors, size: (0,0), starts};

        let mut loc = Location::new(0,0);
        for line in inp.lines() {
            for chr in line.chars() {
                maze.duid_chr(loc, chr);
                loc.x += 1;
            }
            maze.size.0 = maze.size.0.max(loc.x as usize);
            loc.x = 0;
            loc.y += 1;
        }
        maze.size.1 = maze.size.1.max(loc.y as usize);

        maze.keynames.sort();
        maze.keynames.insert(0, '@');
        maze.start =  maze.starts[0];
        
        maze
    }

    /// Split the original maze in 4 and return those 4 mazes in a vector
    /// The original maze is consumed
    /// There are 2 kinds of input maze:\
    /// - a single maze with 1 start that needs to have some walls redrawn 
    /// - an already split maze with 4 starts that only needs to be divided
    pub fn split_into_4(&self) -> Vec<Maze> {

        // The ranges define where paths can be. 
        // Defining these correctly eliminates the new wall locations automatically
        let bx = self.size.0 / 2 + 1;   // boundary x
        let by = self.size.1 / 2 + 1;   // boundary y
        let sx = self.size.0 as usize; // size x
        let sy = self.size.1 as usize; // size y

        // The ranges define where paths can be. 
        // Defining these correctly eliminates the new wall locations automatically
        let ranges: Vec<_> = vec![(1  .. bx-1  , 1      .. by-1),
                                  (bx .. sx - 1, 1      .. by-1),
                                  (1  .. bx-1  , by .. sy - 1),
                                  (bx .. sx - 1, by .. sy - 1),];
        //println!("Ranges: {:?}", ranges);
        
        // The translation vectors for the origins of the new mazes
        // Each maze will have its origin at (0,0). These are the positions that translate to (0,0)
        let translate: Vec<(usize, usize)> = vec![(0,  0), 
                                                  (bx-1, 0), 
                                                  (0,  by-1), 
                                                  (bx-1, by-1)];

        // Define the 4 new start locations
        let starts = if self.starts.len() == 1 {
            vec![Location::new(self.start.x-1, self.start.y-1),
                 Location::new(self.start.x+1, self.start.y-1),
                 Location::new(self.start.x-1, self.start.y+1),
                 Location::new(self.start.x+1, self.start.y+1),]
        } else {
            vec![self.starts[0], self.starts[1],self.starts[2], self.starts[3]]
        };

        // Define 4 copies of the original maze
        let mut mazes: Vec<Maze> = vec![self.clone(), self.clone(), self.clone(), self.clone()];

        // Revamp each of the 4 new maps
        for (n, m) in mazes.iter_mut().enumerate() {
            m.size = (bx, by);

            let transl = translate[n];
            let range = &ranges[n];

            m.start = Location::new(starts[n].x - transl.0 as i32, starts[n].y - transl.1 as i32);

              // Retain only items in the various maps and sets that are in the correct range and translate them
            m.map = m.map.iter()
                         .filter(|loc| range.0.contains(&(loc.x as usize)) && 
                                                 range.1.contains(&(loc.y as usize)) )
                         .map(|loc| Location::new( loc.x - transl.0 as i32, loc.y - transl.1 as i32) )
                         .collect();
            m.keys = m.keys.iter()
                           .filter(|(loc,_)| range.0.contains(&(loc.x as usize)) && 
                                                       range.1.contains(&(loc.y as usize)) )
                           .map(|(loc,v)| (Location::new( loc.x - transl.0 as i32, loc.y - transl.1 as i32), *v) )
                           .collect();
            m.doors = m.doors.iter()
                             .filter(|(loc,_)| range.0.contains(&(loc.x as usize)) && 
                                                         range.1.contains(&(loc.y as usize)) )
                             .map(|(loc,v)| (Location::new( loc.x - transl.0 as i32, loc.y - transl.1 as i32), *v) )
                             .collect();
            m.keys2loc = m.keys.iter()
                               .map(|(k,v)| (*v, *k) )
                               .collect();
            m.keynames = m.keys.iter()
                               .filter(|(_,c)| **c != '@')
                               .map(|(_, v)| *v )
                               .collect();
            m.keynames.sort();
            m.keys.insert(m.start, '@');
            m.keys2loc.insert('@', m.start);
            m.keynames.insert(0, '@');
            m.id = n;

            m.reduce();
        }        
        mazes
    }

    pub fn keynames(&self) -> &Vec<char> {
        &self.keynames
    }

    pub fn location_from_char(&self, key: char) -> Option<&Location> {
        self.keys2loc.get(&key)
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    pub fn starts_len(&self) -> usize {
        self.starts.len()
    }

    pub fn has(&self, loc: Location) -> bool {
        self.map.contains(&loc)
    }

    pub fn key_from_loc(&self, loc: Location) -> Option<&char> {
        self.keys.get(&loc)
    }

    pub fn door_from_loc(&self, loc: Location) -> Option<&char> {
        self.doors.get(&loc)
    }

    pub fn door2key(door: char) -> char {
        door.to_ascii_lowercase()
    }

    fn duid_chr(&mut self, loc: Location, chr: char) {
        match chr {
            '#'=> {},    // Wall: ignore
            '.'|' ' => {
                self.map.insert(loc);
            },
            'a'..='z' => {
                self.map.insert(loc);
                self.keys.insert(loc, chr);  
                self.keys2loc.insert(chr, loc);
                self.keynames.push(chr);
            },
            'A'..='Z' => {
                self.map.insert(loc);
                self.doors.insert(loc, chr);
            },
            '@' => {
                self.map.insert(loc);
                self.keys.insert(loc, chr);
                self.keys2loc.insert(chr, loc);
                self.starts.push(loc);
            },
            _ => panic!("Unknown char {} at {}", chr, loc), 
        }
    }

    /// Reduce changes the maze in place such that all dead ends are removed from the map.
    /// A dead end is characterized by the fact that:
    /// - it is not a wall
    /// - there is key on it
    /// - it has only 1 neighbouring location that is not a wall
    /// Note: a door on a dead end is of no consequence: it never needs being visited
    /// Removal means that the location is turned into a wall. 
    pub fn reduce(&mut self){
        while let Some((loc, nb)) = self.find_dead_end_neigbour() {
            // Found a dead end
            let mut loc = loc;
            let mut nb = nb;
            loop {
                let _ok = self.map.remove(&loc);   // Remove dead end from map
                if let Some(new_nb) = self.dead_end_neighbour(nb) {
                    // Neighbor is now itself a dead end.
                    loc = nb;
                    nb = new_nb;
                } else {
                    // Neighbour is no new dead end
                    break;
                }
            }
        }
    }

    /// Find a dead end.
    /// A dead end is characterized by the fact that: 
    /// - it is not a wall
    /// - there is no key on it
    /// - it has only 1 neighbouring location that is not a wall
    /// Note: a door on a dead end is of no consequence: it never needs being visited
    /// Return value is the dead end itself, and its single neigbbour
    fn find_dead_end_neigbour(&self) -> Option<(Location, Location)> {
        for loc in self.map.iter() {
            if let Some(nb) = self.dead_end_neighbour(*loc) {
                return Some((*loc, nb))
            }
        }
        None
    }

    /// dead_end_neighbour returns None if there is no dead end here.
    /// If loc is on a dead end, its neigbour is returned
    fn dead_end_neighbour(&self, loc: Location) -> Option<Location> {
        if loc == self.start ||
           self.keys.contains_key(&loc) { 
               return None; 
        }
        let mut cnt = 0;
        let mut nb = None;
        let nb1 = Location::new(loc.x - 1, loc.y);
        let nb2 = Location::new(loc.x + 1, loc.y);
        let nb3 = Location::new(loc.x, loc.y - 1);
        let nb4 = Location::new(loc.x, loc.y + 1);

        if self.map.contains(&nb1) { cnt += 1; nb = Some(nb1); }
        if self.map.contains(&nb2) { cnt += 1; nb = Some(nb2); }
        if self.map.contains(&nb3) { cnt += 1; nb = Some(nb3); }
        if self.map.contains(&nb4) { cnt += 1; nb = Some(nb4); }
        if cnt == 1 {
            nb
        } else {
            None
        }
    }

    pub fn paint(&self, ansi: bool) {
        for y in 0..self.size.1 as i32 {
            for x in 0..self.size.0 as i32{
                if ansi {
                    self.paint_col_loc(Location::new(x, y));
                } else {
                    self.paint_loc(Location::new(x, y));
                }
            }
            println!();
        }
    }

    pub fn paint_edge_in_maze(&self, k1: char, k2: char, edge: Vec<Location>) {
        println!("{}->{}: {:?}", k1, k2, edge);
        for y in 0..self.size.1 as i32 {
            for x in 0..self.size.0 as i32{
                let loc = Location::new(x, y);
                self.paint_loc_with_edge(loc, &edge);
            }
            println!();
        }
    }

    fn digit_from(cnt: usize) -> usize {
        let mut cnt = cnt;
        if cnt == 0 {
            return 0;
        }
        loop {
            let d = cnt % 10;
            if d != 0 { return d; }
            cnt /= 10;
        }
    }

    fn paint_loc(&self, loc: Location) {
        if loc == self.start                              { print!("@"); }
        else if let Some(ch) = self.keys.get(&loc)  { print!("{}", *ch); }
        else if let Some(ch) = self.doors.get(&loc) { print!("{}", *ch); }
        else if self.map.contains(&loc)                   { print!(" "); }
        else                                              { print!( "#" ); }
    }

    fn paint_col_loc(&self, loc: Location) {
        if loc == self.start                              { print!("{}", color_key().paint("@")); }
        else if let Some(ch) = self.keys.get(&loc)  { print!("{}", color_key().paint(ch.to_string())); }
        else if let Some(ch) = self.doors.get(&loc) { print!("{}", color_door().paint(ch.to_string())); }
        else if self.map.contains(&loc)                   { print!("{}", color_floor().paint(" ")); }
        else                                              { print!("{}", color_wall().paint(" ") ); }
    }

    fn paint_loc_with_edge(&self, loc: Location, edge: &Vec<Location>) {
        if loc == self.start                              { print!("@"); }
        else if let Some(ch) = self.keys.get(&loc)  { print!("{}", *ch); }
        else if let Some(ch) = self.doors.get(&loc) { print!("{}", *ch); }
        else if self.map.contains(&loc)                   {
            if let Some(cnt) = edge.iter().position(|e| *e == loc ){ 
                print!("{}", Maze::digit_from(cnt) );
            } else {
                print!(" ");
            } 
        } 
        else                                              { print!( "#" ); }
    }
}

impl fmt::Display for Maze {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Map: {}, keys: {}, doors: {}, start: {}, size: ({},{})", 
                self.map.len(), self.keys.len(), self.doors.len(), self.start, self.size.0, self.size.1)
    }
}

