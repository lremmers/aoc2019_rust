use intcode;
use std::collections::HashMap;
use std::fs;

enum Direction {
    Up, 
    Down,
    Left,
    Right,
}

impl Direction {
    fn turn(&self, go_right: bool) -> Self {
        match self {
            Direction::Up    => if go_right { Direction::Right } else { Direction::Left  },
            Direction::Right => if go_right { Direction::Down  } else { Direction::Up    },
            Direction::Down  => if go_right { Direction::Left  } else { Direction::Right },
            Direction::Left  => if go_right { Direction::Up    } else { Direction::Down  },
        }
    }
}

// A panel only needs to know its color
struct Panel {
    is_white: bool,
}

#[derive(PartialEq, Eq, Hash)]
struct Point { 
    x: i32, 
    y:i32,
}

impl Panel {
    fn new() -> Self {
        Panel { is_white: false }
    }
}

fn main() {
    let input = fs::read_to_string("day11_input.txt").unwrap();
	let program: Vec<i64> = input
					.trim()
					.split(',')
					.enumerate()
					.map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();

    //Day 1
    let result = paint_panel(program.clone(), 0);
    println!("Starting at a black spot, painted {} panels", result.len());

    //Day 2
    let result = paint_panel(program, 1);
    println!("Starting at a white spot, painted {} panels", result.len());
    draw_sign(&result);
}

fn paint_panel(program: Vec<i64>, start_color: i64) -> HashMap<Point, Panel> {
    // The grid ties a location to a visited panel
    // The fact that it is in the grid, means it has been visited
    let mut computer =  intcode::Intcode::new(program, vec![]);
    let mut grid: HashMap<Point, Panel> = HashMap::new();
    let mut robot_dir = Direction::Up;      // Start direction
    let mut x = 0;                          // start point
    let mut y = 0;

    let mut ended = false;                  // Stop condition
    while !ended {
        // Make a point of the current location
        let point = Point { x, y };
        // Get the coloer of the current panel
        let this_color = if grid.is_empty() {
                start_color
            } else {
                match grid.get( &point ) {
                    Some(panel) => if panel.is_white {1} else {0},
                    None        => 0,
                }
            };
        computer.push_in(this_color);       // Run the color as input
        let result = computer.run();
        match result {
            Ok(value) => {
                ended = value;
                let outputs = &computer.outputs;
                let last = outputs.len();
                let paint_white = outputs[last-2] == 1;
                let go_right    = outputs[last-1] == 1;
                let mut panel = Panel::new();
                panel.is_white = paint_white;
                grid.insert(point, panel);
                robot_dir = robot_dir.turn(go_right);
                x =  match robot_dir {
                    Direction::Up => x,
                    Direction::Right => x+1,
                    Direction::Down => x,
                    Direction::Left => x-1,
                };
                y =  match robot_dir {
                    Direction::Up => y+1,
                    Direction::Right => y,
                    Direction::Down => y-1,
                    Direction::Left => y,
                };
            },
            Err(_) => ended = true,
        };
    }
    grid
}

fn draw_sign(grid:  &HashMap<Point, Panel>) {
    let mut minx = i32::MAX;
    let mut maxx = i32::MIN;
    let mut miny = i32::MAX;
    let mut maxy = i32::MIN;

    for key in grid.keys() {
        let Point {x, y} = key;
        minx = minx.min(*x);
        maxx = maxx.max(*x);
        miny = miny.min(*y);
        maxy = maxy.max(*y);
    }
    let mut drawing = vec![vec!['.';(maxx+1-minx) as usize];(maxy+1-miny) as usize];
    for (point, panel) in grid {
        if panel.is_white {
            drawing[(maxy-point.y) as usize]
                   [(point.x-minx) as usize] = '#';
        }
    }
    for v in &drawing {
        for w in v {
            print!("{}", w);
        }
        println!("");
    }
}