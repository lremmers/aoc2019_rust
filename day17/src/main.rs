use std::fs;  
use std::io::prelude::*;   
use std::collections::HashSet;
use std::char;
use std::fmt;

use intcode;

fn main() {
    let input = fs::read_to_string("day17_input.txt").unwrap();
    let program: Vec<i64> = input
                    .trim()
                    .split(',')
                    .enumerate()
                    .map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();
    let mut computer = intcode::Intcode::new(program.clone(), vec![]);

    // Part 1
    let mut scaffold = map_scaffolding(&mut computer);
    println!("Scaffold size: {}", scaffold.size() );
    scaffold.find_xings();
    let sum_alignment = scaffold.xing.iter().fold( 0, | acc, e | acc + e.x*e.y);
    println!("Part 1: {}", sum_alignment);

    // Part 2
    // Manually (using spread sheet) mapped out the path and identified A, B and C
    // Main program:  "A,B,A,C,A,B,A,C,B,C\n"  (19)
    // A: "R,4,L,12,L,8,R,4\n"  (16)
    // B: "L,8,R,10,R,10,R,6\n" (17)
    // C: "R,4,R,10,L,12\n"     (13)
    // videofeed "y\n"          (1)

    // It is not entirely clear how the sequence of providing input and rceiving output should work.sum_alignment
    // Attempt: poke, { run, examine output, provide all inputs } *
  
    let input0: Vec<&str> = vec!["A,B,A,C,A,B,A,C,B,C\n",
                                 "R,4,L,12,L,8,R,4\n",
                                 "L,8,R,10,R,10,R,6\n",
                                 "R,4,R,10,L,12\n",
                                 "n\n",
                                 ];
    let input1 = input0.join("");
    let inputs : Vec<i64> = input1.chars().map(|e| e as u32 as i64).collect();
    println!("Inputs: {:?}", inputs);  

    let mut computer = intcode::Intcode::new(program.clone(), inputs);
    computer.poke(0, 2);
    let halted = computer.run().expect("Computer halted after inputs");
    let output_data: String = computer.outputs.iter()
                                      .map(|e| *e as u8 as char)
                                      .collect();
    //let mut file = fs::File::create("foo.txt").unwrap();
    //file.write_all(output_data.as_bytes()).unwrap();
    
    println!("Output: {}", output_data);
    let len = computer.outputs.len();
    println!("Dusted: {:?}", &computer.outputs[len-9..len]);
    if !halted {
        computer.outputs.clear();
        computer.run().unwrap();
        println!("{:?}", computer.outputs);
    }
}

fn scr_home() {
    print!("\x1B[2J"); // cleaer
    print!("\x1B[;H"); // top left
}

fn map_scaffolding(cmptr: &mut intcode::Intcode ) -> Scaffold {
    let retval = cmptr.run();
    let scaffold_data: String = cmptr.outputs.iter()
                                    .map(|e| *e as u8 as char)
                                    .collect();
    println!("{}", scaffold_data);
    Scaffold::new(&scaffold_data)
}

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
struct Location {
    x: i32,
    y: i32,
}
impl Eq for Location {}

impl Location {
    fn new(x: i32, y:i32) -> Self {
        Location {x, y}
    }

    fn from_tpl( tpl: (i32, i32)) -> Self {
        Location { x: tpl.0, y: tpl.1 }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug)]
enum DirT {
    West,
    East,
    North,
    South,
}

#[derive(Debug)]
struct RobotT {
    dir: DirT,
}

impl RobotT {
    fn new<'a>(chr: char) -> Result<Self, String> {
        match chr {
            '<' =>       Ok(Self { dir: DirT::West }),
            '>' =>       Ok(Self { dir: DirT::East }),
            'v' | 'V' => Ok(Self { dir: DirT::South }),
            '^' =>       Ok(Self { dir: DirT::North }),
            _ => Err(format!("Cannot make RobotT from {}", chr)),
        }
    }
}

#[derive(Debug)]
struct Scaffold {
    loc: HashSet<Location>,    // Contains all location with scaffolding
    xing: HashSet<Location>,   // Contains all locations with an intersection: subset of loc
    rloc: Location,            // Robot location
    robot: RobotT,             // Other robot data, like its direction
}

impl Scaffold {
    
    // Make a new scaffolding from a data string
    // The data string may be produced by the computer
    // The end result should
    // - contain a complete location map: self.loc
    // - no xings (crossings) at all
    // - a valid robot location rloc, also in self.loc
    // - a valid self.robot
    // Coordinate (0,0) is top-left.
    // x grows to the right
    // y grows down
    fn new(data: &str) -> Self {
        let mut map = Scaffold { 
                        loc:  HashSet::new(), 
                        xing: HashSet::new(),
                        rloc: Location::new(0,0),
                        robot: RobotT { 
                                dir: DirT::North 
                              },
                      };
        let mut x = 0;
        let mut y = 0;
        for line in data.lines() {
            for chr in line.chars() {
                map.duid_char(chr, Location::new(x, y));
                x += 1;
            }
            y += 1;
            x = 0;
       }
       map
    }
    
    // Interpret a character in the scaffolding-producing data string
    fn duid_char(&mut self, chr: char, loc: Location) {
        match chr {
            '#' => { // Scaffold 
                self.loc.insert(loc);
            },
            '>' | '<' | '^' | 'v' => { // Robot
                self.loc.insert(loc);
                self.rloc = loc;
                self.robot = RobotT::new(chr).expect(&format!("Error building robot at {}", loc));
            }
            '.' => {},  // space, the void: no action 
            _ => {
                    panic!("Unexpected char '{}' on position {}", chr, loc);
                 },
        };
    }

    // Find the size of the map
    fn size(&self) -> Location {
        let mut locmax = Location::new(0,0);
        for loc in &self.loc {
            locmax.x = locmax.x.max(loc.x);
            locmax.y = locmax.y.max(loc.y);
        }
        locmax    
    }

    // finds and records all crossings in the scaffolding map.usize
    // any location with more then 2 neighbours constitutes a crossing
    fn find_xings(&mut self) {
        for loc in &self.loc {
            let mut nb_cnt  = 0;
            if self.loc.contains(&Location::new(loc.x-1, loc.y)) {
                nb_cnt += 1;
            } 
            if self.loc.contains(&Location::new(loc.x+1, loc.y)) {
                nb_cnt += 1;
            } 
            if self.loc.contains(&Location::new(loc.x, loc.y-1)) {
                nb_cnt += 1;
            } 
            if self.loc.contains(&Location::new(loc.x, loc.y+1)) {
                nb_cnt += 1;
            } 
            if nb_cnt > 2 {
                self.xing.insert(*loc);
            }
        }
    }
}
