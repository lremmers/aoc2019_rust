
use std::fmt;

#[derive(Debug)]
pub struct MyError {
    details: String
}

impl MyError {
    pub fn new(msg: String) -> MyError {
        MyError{details: msg.to_string()}
    }
}

impl fmt::Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",&(self.details))
    }
}


