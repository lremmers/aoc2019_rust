use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug)]
struct Ingredient {
    name: String,
    amount: u64,
}

#[derive(Debug)]
struct Recipy {
    output: String,
    amount: u64,
    inputs: Vec<Ingredient>,
}

type Recipies = HashMap<String, Recipy>;
type Stock    = HashMap<String, u64>;

impl Ingredient {
    fn new(descr: &str) -> Self {
        let parts: Vec<_> = descr.split_whitespace().collect();
        let amount: u64 = parts[0].parse().unwrap();
        let name: String = parts[1].to_string();

        Ingredient { amount, name }
    }

    fn amount(&self) -> u64 { self.amount }

    fn name(&self) -> &str { &self.name }
}

impl Recipy {
    fn new(descr: &str) ->  Self {

        let parts: Vec<_> = descr.split(" => ").collect();      // Split in 2 parts
        let ingrs: Vec<_> = parts[0].split(",")
                                    .map(|s| s.trim())
                                    .collect();                 // split part 1 in pieces 

        let output = Ingredient::new(parts[1]);                 // part 2
        let amount = output.amount();
        let output = output.name().to_string();

        let inputs: Vec<_> = ingrs.iter()
                                  .map( |dscr| Ingredient::new(dscr) )
                                  .collect();                   // part 1

        Recipy {
            output,
            amount,
            inputs,
        }
    }

    /// Make the required quanity of the self stuff
    /// Use ingredients available in the stock, make the rest.
    /// At least the amount mentioned in the recipy must be made
    /// If you must make more then required, stock the rest of the stuff before returning
    /// Any ore you use or is used in other recipies must be recorded as return value.
    fn make(&self, quant: u64, recipies: &Recipies, stock: &mut Stock) -> u64 {
        let mut ore = 0_u64;
        let to_make = &self.output;         // make this stuff
        let in_stock = *stock.get(to_make).unwrap();
        let mut batch = 0;
        let quant_make = if in_stock < quant {
                            *stock.get_mut(to_make).unwrap() = 0;
                            batch = (((quant-in_stock) as  f64)/(self.amount as f64)).ceil() as u64;
                            self.amount * batch      // this much you must make
                        } else {
                            *stock.get_mut(to_make).unwrap() -= quant;
                            0
                        };
        //print!("\nMaking {} of {} using {} in stock and fabr. {} ", quant, self.output, in_stock, quant_make);
        if quant_make > 0 {
            for ingr in &self.inputs {
                let to_use = ingr.name();           // with this stuff
                let quant_use = ingr.amount() * batch;      // using this much
                //print!("with {} of {}, ", quant_use, to_use);

                if to_use == "ORE" {
                    ore += quant_use;
                    // set stock[ORE} to needed - quant
                } else {
                    ore += recipies.get(to_use).unwrap().make(quant_use, recipies, stock);
                }
            }
            let exces = quant_make + in_stock - quant;
            //println!(" with {} ore, stocking {}, from: {}", ore, exces, self);
            *stock.get_mut(to_make).unwrap() += exces;
        }        
        ore
    }

    fn new_stock(&self, recipies: &Recipies) ->  Stock {
        let mut stock: Stock = HashMap::new();
        for (k, _) in recipies.iter() {
            stock.insert(k.to_string(), 0);
        }
        stock
    }

    fn max_available(&self, ore: u64, ore4fuel: u64, recipies: &Recipies) -> u64 {
        println!("Start with {}", ore4fuel);
        let mut cnt = 100;
        let mut attempt = ore / ore4fuel;                              // FUEL: Too small: small numbers --> bad stock use
        loop {
            let mut stock = self.new_stock(recipies);
            let ore4attempt = self.make(attempt, recipies, &mut stock);  // ore needed for attempt FUEL
            let ore4fuel = ore4attempt.min(ore)/attempt;
            print!("attempt:{} ore4attempt: {} ore4fuel: {} ->  ", attempt, ore4attempt, ore4fuel);
            if ore4attempt > ore {
                let delta = ore4attempt-ore;
                if delta < ore4fuel {
                    return attempt - 1;
                }
                attempt -= delta/ore4fuel; 
            } else {
                let delta =  ore - ore4attempt;
                if delta < ore4fuel {
                    return attempt;
                }
                attempt += delta/ore4fuel;
            }
            println!("{}", attempt);
            cnt -= 1;
            if cnt <= 0 {
                return 0;
            }
        }
    }
}

impl fmt::Display for Ingredient {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} of '{}'", self.amount, self.name)
    }
}

impl fmt::Display for Recipy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = format!("For {} of '{}' use: ", self.amount, self.output);
        for inp in &self.inputs {
            res.push_str(&format!("{} ", inp));
        };
        write!(f, "{}", res)
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = File::open("day14_input.txt")?;
    let buffered = io::BufReader::new(input);

    let mut recipies: Recipies = HashMap::new();
    let mut stock: Stock = HashMap::new();

    for line in buffered.lines() {
        let desc = line.unwrap();
        let recipy = Recipy::new(&desc); 
        stock.insert(recipy.output.clone(), 0);
        recipies.insert(recipy.output.clone(), recipy);
    }

    let ore = recipies.get("FUEL").unwrap().make(1, &recipies, &mut stock);
    println!("{} of ORE is needed for 1 of FUEL", ore);
    let mf = recipies.get("FUEL").unwrap().max_available(1_000_000_000_000, ore, &recipies);
    println!("{} of FUEL can be made with 1_000_000_000_000 ORE", mf);
    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;

    fn do_test(lines: &str) -> u64 {
        let mut recipies: Recipies = HashMap::new();
        let mut stock: Stock = HashMap::new();
        for line in lines.lines() {
            let recipy = Recipy::new(&line);
            stock.insert(recipy.output.clone(), 0);
            recipies.insert(recipy.output.clone(), recipy);
        }
        recipies.get("FUEL").unwrap().make(1, &recipies, &mut stock)
    }

    fn max_fuel_test(ore: u64, ore4fuel: u64, lines: &str) -> u64 {
        let mut recipies: Recipies = HashMap::new();
        for line in lines.lines() {
            let recipy = Recipy::new(&line);
            recipies.insert(recipy.output.clone(), recipy);
        }
        recipies.get("FUEL").unwrap().max_available(ore, ore4fuel, &recipies)
    }

    #[test]
    fn test1() {
        let lines = 
"10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL";
        let ore = do_test(lines);
        assert_eq!(ore, 31);
    }

    #[test]
    fn test2() {
        let lines = 
"9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL";
        let ore = do_test(lines);
        assert_eq!(ore, 165);
    }

    #[test]
    fn test3() {
        let lines = 
"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
        let ore = do_test(lines);
        assert_eq!(ore, 13312);
        let mf = max_fuel_test(1_000_000_000_000, ore, lines);
        assert_eq!(mf, 82892753)
    }

    #[test]
    fn test4() {
        let lines = 
"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF";
        let ore = do_test(lines);
        assert_eq!(ore, 180697);
        let mf = max_fuel_test(1_000_000_000_000, ore, lines);
        assert_eq!(mf, 5586022 )
    }

    #[test]
    fn test5() {
        let lines = 
"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX";
        let ore = do_test(lines);
        assert_eq!(ore, 2210736);
        let mf = max_fuel_test(1_000_000_000_000, ore, lines);
        assert_eq!(mf, 460664 )
    }
}
