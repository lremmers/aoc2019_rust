use std::fmt;
use std::collections::{ HashSet, HashMap };
use std::char;

use ansi_term::{ Colour,Style };

pub fn print_maze_str(mstr: &str) {
    println!("{}",mstr);
}

pub fn print_maze_str_coloured(mstr: &str) {
    for  chr in mstr.chars() {
        match chr {
            '#'       => print!("{}", color_wall().paint(" ")),             // wall
            '.'       => print!("{}", color_floor().paint(" ")),            // walkway
            'A'..='Z' => print!("{}", color_door().paint(chr.to_string())), // zap
            _         => print!("{}", chr), 	                            // anything else 
        }
    }
    println!();
}

fn color_wall() -> Style {
    Colour::Black.dimmed().on(Colour::Fixed(242)).dimmed()
}

fn color_floor() -> Style {
    Colour::Black.on(Colour::White)
}

fn color_key() -> Style {
    Colour::Red.bold().on(Colour::White)
}

fn color_door() -> Style {
    Colour::Blue.bold().on(Colour::White)
}

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub struct Location {
    pub x: usize,
    pub y: usize,
}
impl Eq for Location {}

impl Location {
    pub fn new(x: usize, y:usize) -> Self {
        Location {x, y}
    }

    pub fn from_tpl( tpl: (usize, usize)) -> Self {
        Location { x: tpl.0, y: tpl.1 }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub struct Location3 {
    pub x: usize,
    pub y: usize,
    pub z: usize,
}
impl Eq for Location3 {}

impl Location3 {
    pub fn new(x: usize, y:usize, z: usize) -> Self {
        Location3 {x, y, z}
    }

    pub fn from_loc(loc: Location, lvl: usize) -> Self {
        Location3 {  x: loc.x, y: loc.y, z: lvl }
    }

    pub fn from_tpl( tpl: (usize, usize, usize)) -> Self {
        Location3 { x: tpl.0, y: tpl.1, z: tpl.2 }
    }
}

impl fmt::Display for Location3 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum DirT {
    West,
    East,
    North,
    South,
}

impl DirT {
    pub fn iterator() -> impl Iterator<Item = DirT> {
        [DirT::North, DirT::South, DirT::East, DirT::West].iter().copied()
    }
}

/// Attributes for an edge
#[derive(Clone, Debug, Copy)]
pub struct Edge {
    pub from: Location,
    pub to: Location,
    pub dist: usize,            // Weigth of the edge: distance to travel it.
    //pub path: Vec<Location>,
}

#[derive(Clone, Debug, Copy)]
pub struct Edge3 {
    pub from: Location3,
    pub to: Location3,
    pub dist: usize,            // Weigth of the edge: distance to travel it.
    //pub path: Vec<Location>,
}

// A portal is tied to its location.
// There are with the same name'
// These 2 zap toawards each others neighbouring cell
#[derive(Clone, Debug)]
pub struct Portal {
    pub name: String,       // label of the portal
    pub c: char,                // 1-char other name of the portal, for easy printing 
    zloc: Location,         // other location for the Portal: the location of the other side  
    pub mloc: Location,     // maze location for this side of the portal
}

#[derive(Clone, Debug)]
pub struct Portal3 {
    pub name: String,       // label of the portal
    pub c: char,                // 1-char other name of the portal, for easy printing 
    zloc: Location3,         // other location for the Portal: the location of the other side  
    pub mloc: Location3,     // maze location for this side of the portal
}


impl Portal {
    fn new(name: &str, c: char, zloc: Location, mloc: Location) -> Portal {
        Portal {
            name: name.to_string(),
            c,
            zloc,
            mloc
        }
    }

    pub fn zap(&self) -> Location {
        self.zloc
    }
}

impl Portal3 {
    fn new(name: &str, c: char, zloc: Location3, mloc: Location3) -> Portal3 {
        Portal3 {
            name: name.to_string(),
            c,
            zloc,
            mloc
        }
    }

    pub fn zap(&self) -> Location3 {
        self.zloc
    }
}


// A Maze captures a 2D-maze in a set of locations and portal lcation pairs
// The upper-left location is (0,0)
// If size == (x,y) then the lower-right locations is (x-1, y-1)
// The maze is surrounded by walls and portal locations. There is also an "outer" edge in the middle
// also with walls and portal locations
// Each portal location has a 2 character name. 
// Portal names on top or bottom walls are written top to bottom above or under their portal location
// Portal names on left or right walls are written left to right besides their portal location
#[derive(Clone)]
pub struct Maze {
    pub map: HashSet<Location>,             // All walkable locations in the maze
    pub portals: HashMap<Location, Portal>,     // Portals
    pub start: Location,
    pub target: Location,
    size: Location,                         // The size of the map in (x, y) direction, coded as Location
}

// Struct to monitor state for the building of the portals
struct PortalBuild {
    inner_tl: Location,                     // Top left corner of inner region
    inner_br: Location,                     // Bottom right corner of inner region
                                            // The x is determined first, the y much later
    at_lower1: bool,                        // found a 1st letter of the bottom inner region
    at_lower2: bool,                        //     
    cnt: usize,                             // number of entries on this line
    portals: HashMap<Location, String>,     // Location next to the maze
    mazeloc: HashMap<Location, Location>,   // Location in the maze
}

impl Maze {
    pub fn new(inp: &str) -> Self {
        let map = HashSet::new();
        let portals = HashMap::new();
        let start = Location::new(0,0);
        let target = Location::new(0,0);

        let mut maze = Self { map, portals, size: Location::new(0,0), start, target };
        let mut pb = PortalBuild { inner_tl: Location::new(0,0),
                                   inner_br: Location::new(0,0),
                                   at_lower1: false,
                                   at_lower2: false,
                                   cnt: 0,
                                   portals: HashMap::new(),
                                   mazeloc: HashMap::new(),
                                };

        let mut loc = Location::new(0,0);
        for line in inp.lines() {
            for chr in line.chars() {
                maze.duid_chr(loc, chr, &mut pb);
                loc.x += 1;
            }
            loc.x = 0;
            loc.y += 1;
        }
        maze.collect_portals(&pb);
        maze
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    fn duid_chr(&mut self, loc: Location, chr: char, pb: &mut PortalBuild) {
        match chr {
            '#' => {},    // Wall or outside maze: ignore
            '.' => { 
                self.map.insert(loc);
            },
            'A'..='Z'|' ' => {
                self.make_portal(chr, loc, pb);
            },
            _ => panic!("Unknown char {} at {}", chr, loc), 
        }
    }

    // Make portal creates portals.
    // A portal is pair of capital letters embedded in spaces at three sides
    // The spaces define the rows or columns where portal letters can be.print!
    // - The first two rows (loc.y == 0 || loc.y == 1) can only contain spaces and portal labels
    //   for portals to the South of the label
    // - The first two columns (loc.x == 0 || loc.x == 1) can only contain spaces and portal labels
    //   for portals to the East of the label
    // - row and columns 3 contain only walls and Portal locations. The first space after that 
    //   identifies the end of the maze and further portal label region
    // - If the size of the maze is N, the first portal label col/row at the end side is N + 2
    // - When column/row > 2 and a space is found before size, this is the start of the inner region 
    // - When a wall is found on the first row after the inner_tl, this defines x of inner_br
    // - When a wall is found at inner_tl.x + 1, this defines inner_br.y
    //   Two rows before that, portal labels may be found
    fn make_portal(&mut self, chr: char, loc: Location, pb: &mut PortalBuild) {
        //println!("{} {} {} {} {}", loc, pb.inner_tl, pb.inner_br, self.size, chr);
        pb.cnt += 1;
        if loc.y == 0 && chr != ' ' {
            // top side 1st letters
            let ploc = Location::new(loc.x, 1);
            pb.portals.insert(ploc, chr.to_string());
        } else if loc.y == 1 && chr != ' ' {
            // top side 2nd letters
            let ploc = Location::new(loc.x, 1);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Must be there after loc.y == 1
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x, 2));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 
        } else if loc.y == 2  && loc.x > 3 && chr == ' ' {
            // Determine x size
            self.size.x = loc.x - 3

        } else if loc.x == 0 && chr != ' ' {
            // left side 1st letters
            let ploc = Location::new(1, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if loc.x == 1 && chr != ' ' {
            // left side 2nd letters
            let ploc = Location::new(1, loc.y);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Must be there after loc.x == 1
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(2, loc.y));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 

        } else if loc.y > 3 &&  loc.x > 3 && loc.x < self.size.x && pb.inner_tl.x == 0 && chr == ' ' {
            // We are at inner_tl
            pb.inner_tl = loc;
        } else if pb.inner_tl.y != 0 && loc.y == pb.inner_tl.y && loc.x > pb.inner_tl.x && loc.x < self.size.x && chr == ' ' {
            // Still in inner region: push x of inner_br to this x
            pb.inner_br.x = loc.x;
        } else if pb.inner_tl.y != 0 && loc.y == pb.inner_tl.y && chr != ' ' {
            // Upper row of inner region, 1st letter
            let ploc = Location::new(loc.x, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if pb.inner_tl.y != 0 && loc.y == pb.inner_tl.y + 1 && chr != ' ' &&  loc.x != pb.inner_tl.x {
            // Upper row of inner region, 2nd letter
            let ploc = Location::new(loc.x, loc.y-1);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x, loc.y-2));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 

        } else if pb.inner_tl.x != 0 && loc.x == pb.inner_tl.x && chr != ' ' {
            // Left row of inner region, 1st letter
            let ploc = Location::new(loc.x, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if pb.inner_tl.x != 0 && loc.x == pb.inner_tl.x + 1 && chr != ' ' &&  loc.x != pb.inner_tl.y {
            // Left row of inner region, 2nd letter
            let ploc = Location::new(loc.x-1, loc.y);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x-2, loc.y));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 

        } else if pb.inner_tl.y > 0 && loc.y >= pb.inner_tl.y + 1 && loc.x == pb.inner_br.x - 1 && chr != ' ' {
            // right side of inner region, 1st letter
            let ploc = Location::new(loc.x+1, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if pb.inner_tl.y > 0 && loc.y >= pb.inner_tl.y + 1 &&  loc.y < pb.inner_br.y + 1      
                                    && loc.x == pb.inner_br.x && chr != ' ' {
            // Left side of inner region, 2nd letter
            let ploc = Location::new(loc.x, loc.y);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x+1, loc.y));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 

        } else if pb.inner_tl.x > 0 && loc.x > pb.inner_tl.x+1 && loc.x < pb.inner_br.x - 1 
                                    && loc.y > pb.inner_tl.y + 2 && !pb.at_lower2  
                                    && loc.y <= pb.inner_br.y  && chr != ' ' {
            // bottom side of inner region, 1st letter
            pb.at_lower1 = true;
            let ploc = Location::new(loc.x, loc.y+1);
            pb.portals.insert(ploc, chr.to_string());
            //println!("Inserted {}", ploc);
        } else if pb.at_lower1 && !pb.at_lower2 && loc.x == 0 {
            pb.at_lower2 = true;
        } else if pb.at_lower1 && pb.at_lower2 && loc.x == 0 {
            pb.at_lower1 = false;
        } else if pb.at_lower1 && pb.at_lower2 && loc.x >= pb.inner_tl.x + 1 && loc.x < pb.inner_br.x - 1 && chr != ' ' {
            // bottom side of inner region, 2nd letter
            let ploc = Location::new(loc.x, loc.y);
            //println!("{}", ploc); for (p,_) in pb.portals.iter() { print!("{}", p);};
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x, loc.y+1));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 

        } else if pb.inner_tl.x > 0 && loc.y == pb.inner_tl.y  {
            // Still in inner region: push y of inner_br to this y
            pb.inner_br.y = loc.y;
        } else if pb.inner_br.y > 0  && loc.y == pb.inner_br.y + 1 && pb.cnt > 4 {
            pb.inner_br.y = loc.y;
        } else if pb.inner_br.y > 0 && loc.x > pb.inner_tl.x + 3 && pb.cnt <= 2 {
            self.size.y = loc.y - 1

        } else if loc.y > 3 && self.size.x > 0 && loc.x == self.size.x + 2 && chr != ' ' {
            // outer right side 1st letter
            let ploc = Location::new(loc.x, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if loc.y > 3 && self.size.x > 0 && loc.x == self.size.x + 3 && chr != ' ' {
            // bottom side of inner region, 2nd letter
            let ploc = Location::new(loc.x-1, loc.y);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            pb.mazeloc.insert(ploc, Location::new(loc.x-2, loc.y));
            label.push(chr);
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 
        
        } else if loc.x > 3 && self.size.y > 0 && loc.y == self.size.y + 2 && chr != ' ' {
            // outer bottom side 1st letter
            let ploc = Location::new(loc.x, loc.y);
            pb.portals.insert(ploc, chr.to_string());
        } else if loc.x > 3 && self.size.y > 0 && loc.y == self.size.y + 3 && chr != ' ' {
            // bottom side of outer region, 2nd letter
            let ploc = Location::new(loc.x, loc.y-1);
            let label = pb.portals.get_mut(&ploc).unwrap();     // Should be there after 1st letter
            label.push(chr);
            pb.mazeloc.insert(ploc, Location::new(loc.x, loc.y-2));
            println!("Label {} at {}/{}", label, ploc, pb.mazeloc[&ploc]); 
        }
        if loc.x == 0 {
            pb.cnt = 0
        }
    }

    // Given the PortalBuild compiled scanning the maze string, now collect all the portal info
    // For all portals except AA and ZZ, there should be two locations. They should all belong to the maze
    fn collect_portals(&mut self, pb: &PortalBuild) {
        let mut revmap: HashMap<String, Location> = HashMap::new();
        let mut cnt = 1;
        for (ploc, name) in pb.portals.iter() {
            if *name == "AA" {
                self.start = pb.mazeloc[&ploc];
                println!("Start location:  {}", self.start);    
            } else if *name == "ZZ" {
                self.target = pb.mazeloc[&ploc];
                println!("Target location: {}", self.target);
            } else {
                if let Some(ploc2) = revmap.get(name) {
                    // We have a pair of portals
                    let c = char::from_digit(cnt, 36).unwrap();
                    let mazeloc1 = pb.mazeloc[ploc];
                    let mazeloc2 = pb.mazeloc[ploc2];
                    let pp = Portal::new(name, c, *ploc2, mazeloc1);
                    self.portals.insert(*ploc, pp);

                    let pp = Portal::new(name, c, *ploc, mazeloc2);
                    self.portals.insert(*ploc2, pp);
                    cnt += 1;
                } else {
                    revmap.insert(name.to_string(), *ploc);
                }
            }
        }
        //println!("{:?}", self.portals);
    }

    /// Reduce changes the maze in place such that all dead ends are removed from the map.
    /// A dead end is characterized by the fact that:
    /// - it is not a wall
    /// - it has only 1 neighbouring location that is not a wall
    /// Removal means that the location is turned into a wall. 
    pub fn reduce(&mut self){
        while let Some((loc, nb)) = self.find_dead_end_neigbour() {
            // Found a dead end
            let mut loc = loc;
            let mut nb = nb;
            loop {
                let _ok = self.map.remove(&loc);   // Remove dead end from map
                if let Some(new_nb) = self.dead_end_neighbour(nb) {
                    // Neighbor is now itself a dead end.
                    loc = nb;
                    nb = new_nb;
                } else {
                    // Neighbour is no new dead end
                    break;
                }
            }
        }
    }

    /// Find a dead end.
    /// A dead end is characterized by the fact that: 
    /// - it is not a wall
    /// - it has only 1 neighbouring location that is not a wall
    /// Return value is the dead end itself, and its single neigbbour
    fn find_dead_end_neigbour(&self) -> Option<(Location, Location)> {
        for loc in self.map.iter() {
            if let Some(nb) = self.dead_end_neighbour(*loc) {
                return Some((*loc, nb))
            }
        }
        None
    }

    /// dead_end_neighbour returns None if there is no dead end here.
    /// If loc is on a dead end, its neigbour is returned
    fn dead_end_neighbour(&self, loc: Location) -> Option<Location> {
        if loc == self.start {
               return None; 
        }
        let mut cnt = 0;
        let mut nb = None;
        let nbs = [Location::new(loc.x - 1, loc.y    ),
                   Location::new(loc.x + 1, loc.y    ),
                   Location::new(loc.x    , loc.y - 1),
                   Location::new(loc.x    , loc.y + 1),
                  ];

        for n in &nbs {
            if self.map.contains(n) { 
                cnt += 1; nb = Some(*n);
            }
            if self.portals.contains_key(n) {
                cnt += 1; nb = Some(*n);
            }
            if *n == self.start {
               cnt += 1; nb = Some(*n)
            }    
            if *n == self.target {
                cnt += 1; nb = Some(*n)
            }    
        }
        if cnt <= 1 {
            nb
        } else {
            None
        }
    }

    pub fn paint(&self, ansi: bool) {
        for y in 0..self.size.y+4 {
            for x in 0..self.size.x+4 {
                if ansi {
                    self.paint_col_loc(Location::new(x, y));
                } else {
                    self.paint_loc(Location::new(x, y));
                }
            }
            println!();
        }
    }

    fn paint_loc(&self, loc: Location) {
        if loc == self.start                              { print!("A");  return;}
        if loc == self.target                             { print!("Z");  return; }
        else if self.map.contains(&loc)                   { print!(" ");  return; }
        else if self.portals.contains_key(&loc)           { print!("{}", self.portals[&loc].c);  return; }  
        else                                              { print!("#");  return; }
    }

    fn paint_col_loc(&self, loc: Location) {
        if loc == self.start                              { print!("{}", color_key().paint("A")); }
        else if loc == self.target                        { print!("{}", color_key().paint("Z")); }
        else if self.map.contains(&loc)                   { print!("{}", color_floor().paint(" "))}
        else if self.portals.contains_key(&loc)           { print!("{}", color_key().paint(format!("{}", self.portals[&loc].c))); }  
        else                                              { print!("{}", color_wall().paint(" ") ); }
    }



}

impl fmt::Display for Maze {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Map: {}, size: ({},{}, start: {}, target: {})", 
                self.map.len(), self.size.x, self.size.y, self.start, self.target)
    }
}

