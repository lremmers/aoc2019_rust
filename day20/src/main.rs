pub mod maze;

use std::fs;
use std::fmt;
use std::collections::{HashSet, HashMap, VecDeque};
use std::time::{Instant};
//use itertools::join;
use clap;

use maze::{ Maze, DirT, Location, Location3};

fn main() {
    let matches = clap::App::new("Pluto maze solver")
                    .version("0.0")
                    .author("Len Remmerswaal")
                    .about("Aoc2019 Day20 Part 1")
                    .arg(clap::Arg::with_name("input")
                            .help("Holds maze definition")
                            .index(1))
                    .arg(clap::Arg::with_name("color")
                            .help("print a ANSI coloured map i.o. a char map")
                            .short("c")
                            .long("color")
                            .takes_value(false))
                    .arg(clap::Arg::with_name("reduce_map")
                            .help("Find a reduced map")
                            .short("r")
                            .long("reduce")
                            .takes_value(false))
                    // .arg(clap::Arg::with_name("edges")
                    //         .help("Print all edges (lots of output!)")
                    //         .short("e")
                    //         .long("edges")
                    //         .takes_value(false))
                    // .arg(clap::Arg::with_name("part2")
                    //         .help("Perform part 2 of the puzzle")
                    //         .short("2")
                    //         .takes_value(false))
            .get_matches();

    let fname = matches.value_of("input").unwrap_or("day20_input.txt");
    let mazestr = fs::read_to_string(fname).expect(&format!("Cannot read file {}", fname));

    if matches.is_present("color") {
        maze::print_maze_str_coloured(&mazestr);
    } else {
        maze::print_maze_str(&mazestr);
    }

    let now = Instant::now();
    let mut maze = Maze::new(&mazestr);
    println!("Create maze in {}s", now.elapsed().as_secs_f32());
    println!("{}", maze);

    if matches.is_present("reduce_map"){
        let now = Instant::now();
        let cursize = maze.len();
        maze.reduce();
        if maze.len() != cursize {
            maze.paint( matches.is_present("color"));
        }
        println!("Reduce maze in {}s", now.elapsed().as_secs_f32());
    }

    part1(maze.clone());
    part2(maze);
}

fn part1(maze: Maze) {
    let nw = Network::new(maze);
    let (len, pred) = nw.find_path(nw.maze.start, nw.maze.target);
    print!("Walk {} from AA to ZZ along ", len);
    println!("{}", nw.path_to_string(pred));
}

fn part2(maze: Maze) {
    let nw = Network::new(maze);
    let (len, pred) = nw.find_path3(nw.maze.start, nw.maze.target);
    print!("Walk {} from AA to ZZ along ", len);
    //println!("{}", nw.path_to_string(pred));

}

/// Network
/// The network is a number of nodes (start, target and portals)
/// with edges between them.
/// Start is at the location of AA itself. Arriving is at ZZ itself.
struct Network {
    //The nodes. 
    // Leaving a portal node is from its maze location. Arriving is at the portal's location
    // Leaving the start location is 1 step
    nodes: HashSet<Location>,

    // An edge is from a node to a node and has a length.
    // If an edge is bidrecctional, there are two defined.
    // The first Location is the from location.
    // edges: HashMap<(Location, Location), Edge>,
    edges: HashMap<Location, HashMap<Location, Edge>>,

    // Reference to the used maze
    maze: Maze,

}

/// Edge
/// an Edge is from a location to a location.
/// A bidrectional edge is registerd twice with to and from reveresed, and the same len 
#[derive(Clone, Debug, Copy)]
pub struct Edge {
    pub from: Location,
    pub to: Location,
    pub len: usize,            // Weigth of the edge: distance to travel it.
    //pub path: Vec<Location>,
}

impl fmt::Display for Edge {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}->{}:{}", self.from, self.to, self.len)
    }
}

struct QueData {
    loc: Location,
    dist: usize,
}
impl QueData {
    fn new( loc: Location, dist: usize) -> Self {
        Self { loc, dist }
    }
}

impl Network {
    fn new( maze: Maze) -> Network {
        let nodes = HashSet::new();
        let edges = HashMap::new();
        let mut nw = Network { maze, nodes, edges };
        
        nw.nodes.insert(nw.maze.start);
        nw.nodes.insert(nw.maze.target);
        for (loc, _) in nw.maze.portals.iter() {
            nw.nodes.insert(*loc);
        }

        nw.find_edges();
        nw
    }

    /// Find all edges in the network
    fn find_edges(&mut self) {
        for n in self.nodes.iter() {
            //println!("Node: {}", *n);
            self.edges.insert(*n, self.construct_edges_from(*n));
        }
    }

    /// Find the edges that lead from node n
    /// Use a BFS routine to find the shortest distances to all available maze segment ends.
    /// From each location, 4 directions are tried.
    /// The state is kept in a VecDeque 
    /// A portal has a 1 step edge to its counterpart
    fn construct_edges_from(&self, start: Location) -> HashMap<Location, Edge> {

        let mut queue: VecDeque<QueData> = VecDeque::new();    // locations to handle
        let mut visited: HashSet<Location> = HashSet::new();   // locations visited, with distance from start
        let mut edges = HashMap::new();
        
        // Start walking from the maze location of a portal
        let startm = if self.maze.portals.contains_key(&start) {
                        self.maze.portals[&start].mloc
                     } else {
                         start
                     };
        visited.insert(startm);
        queue.push_back( QueData::new(startm, 0));    // start with  searching from start
        while let Some(base) = queue.pop_front() {    // base is the location being handled
            visited.insert(base.loc);               // This location is now handled
            //println!(" At {} with dist {}", base.loc, base.dist);
            for dir in DirT::iterator() {
                let testloc = match dir {
                    DirT::North => Location::new(base.loc.x , base.loc.y-1),
                    DirT::South => Location::new(base.loc.x , base.loc.y+1),
                    DirT::West => Location::new(base.loc.x-1, base.loc.y),
                    DirT::East => Location::new(base.loc.x+1, base.loc.y),
                };
                //print!("Trying {}", testloc);
                if visited.contains(&testloc) {
                    //println!(": visited");
                    continue;
                }
                if !self.maze.map.contains(&testloc) &&
                   !self.maze.portals.contains_key(&testloc) &&
                   testloc != self.maze.start && testloc != self.maze.target {
                       // This location cannot be walked on.
                       //println!(": no maze");
                       continue;
                };
                let is_portal = self.maze.portals.contains_key(&testloc);
                // if is_portal {
                //     print!("Portal at dist {}", base.dist);
                // }
                if base.dist == 0 && is_portal {
                    // This is the portal belonging to the mazeloc. Add a 1 step edge.
                    let target =  self.maze.portals[&testloc].zap();
                    // println!(" Zap to {}", target);
                    let thisedge =  Edge { from: start, to: target, len: 1 };
                    edges.insert(target, thisedge);
                    continue
                } 
                if is_portal  
                   || testloc == self.maze.start 
                   || testloc == self.maze.target {
                       // This is a target, but not the local zap location. Record the edge.
                       let the_dist =  if is_portal { base.dist } else { base.dist + 1};
                    //    println!(" End at {}, dist {}", testloc, the_dist);
                       let thisedge =  Edge { from: start, to: testloc, len: the_dist };
                       edges.insert(testloc, thisedge);
                       continue
                };
                queue.push_back( QueData::new(testloc, base.dist+1));
            }
        }
        //println!("edges: {:#?}", edges);
        //Network::print_edges(start, &edges);
        edges       
    }

    fn print_edges(loc: Location, edges: &HashMap<Location, Edge>) {
        for (k,v) in edges.iter(){
            println!("{}/{}: {}", loc, k, v);
        }
    }

    /// Find the shortest path through the network from 'from' to 'to'
    /// This is done using the Dijkstra algorithm
    /// -  
    fn find_path(&self, from: Location, to: Location) -> (usize, HashMap<Location, Location>) {

        let mut result = 0;
        let mut unvisited = self.nodes.clone();
        let mut pred: HashMap<Location, Location> = HashMap::new();
        //println!("Start with {:?}", unvisited);
        let mut distances : HashMap<_, _> = unvisited.iter()
                                                     .map(|n| (*n, usize::MAX))
                                                     .collect();
        *distances.get_mut(&from).unwrap() = 0;
        while !unvisited.is_empty() {
            let (&thisn, &dist0) = distances.iter()
                                            .filter(|(loc,_)| unvisited.contains(loc))
                                            .min_by_key(|(_, d)| **d ).unwrap();
            unvisited.remove(&thisn);     // This node is done
            //println!("Visit {} at {}, left: {:?}", thisn, dist0, unvisited);
            // find all edges from thisn
            let edges = &self.edges[&thisn];
            for (&trgt, edge) in edges.iter() {
                //print!("Try {} ", trgt);
                if !unvisited.contains(&trgt) {
                    //println!(" visited");
                    continue;           // Allready visited
                }
                let len = edge.len;
                if let Some(dist1) = distances.get_mut(&trgt) {
                    if dist0 + len < *dist1 {
                        //println!(" dist of {} was {} set to {}", trgt, dist1, dist0 + len);
                        *dist1 = dist0 + len;
                        pred.insert(trgt, thisn);
                        if trgt == to {
                            result = *dist1;
                        }
                    } else {
                        //println!(" {} > {}: skipped", dist0+len, dist1)
                    }
                }
            }
        }
        (result, pred)
    }

    fn find_path3(&self, from: Location, to: Location) -> (usize, HashMap<Location3, Location3>) {

        let mut result = 0;
        let mut unvisited = self.nodes.clone();
        let mut pred: HashMap<Location3, Location3> = HashMap::new();
        //println!("Start with {:?}", unvisited);
        let mut distances : HashMap<_, _> = unvisited.iter()
                                                     .map(|n| (*n, usize::MAX))
                                                     .collect();
        *distances.get_mut(&from).unwrap() = 0;
        while !unvisited.is_empty() {
            let (&thisn, &dist0) = distances.iter()
                                            .filter(|(loc,_)| unvisited.contains(loc))
                                            .min_by_key(|(_, d)| **d ).unwrap();
            unvisited.remove(&thisn);     // This node is done
            //println!("Visit {} at {}, left: {:?}", thisn, dist0, unvisited);
            // find all edges from thisn
            let edges = &self.edges[&thisn];
            for (&trgt, edge) in edges.iter() {
                //print!("Try {} ", trgt);
                if !unvisited.contains(&trgt) {
                    //println!(" visited");
                    continue;           // Allready visited
                }
                let len = edge.len;
                if let Some(dist1) = distances.get_mut(&trgt) {
                    if dist0 + len < *dist1 {
                        //println!(" dist of {} was {} set to {}", trgt, dist1, dist0 + len);
                        *dist1 = dist0 + len;
                        pred.insert(trgt, thisn);
                        if trgt == to {
                            result = *dist1;
                        }
                    } else {
                        //println!(" {} > {}: skipped", dist0+len, dist1)
                    }
                }
            }
        }
        (result, pred)
    }


    fn path_to_string(&self, pred: HashMap<Location, Location>) -> String {
        let mut current = self.maze.target;
        let mut result = format!("{}:ZZ", current);
        while let Some(loc) = pred.get(&current) {
            if let Some(portal) = self.maze.portals.get(loc) {
                result = format!("{}:{}/{}, {}", loc, portal.name, portal.c, result)              
            } else {
                result = format!("{}:AA, {}", loc, result)              
            }
            current = *loc;
        }
        result
    }
}

