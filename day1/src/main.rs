// Read file
// For every line:
//    parse integer
//    determine floor(x/3)-2
//    add all together
// print result

use std::io;
use std::io::prelude::*;
use std::fs::File;

fn main() -> io::Result<()> {
	{ // Day 1
		let f = File::open("day1_fuel_load.txt").unwrap();
		let f = io::BufReader::new(f);
		let wt:i32 = f.lines()
			 .map(|s| -> i32 {s.unwrap().parse().unwrap() })
			 .map(|x| x/3-2)
			 .sum();
		println!("Modules gewicht: {}", wt);
	}
	// Day 2
	{
		let f = File::open("day1_fuel_load.txt").unwrap();
		let f = io::BufReader::new(f);
		let wt:i32 = f.lines()
			.map(|s| -> i32 {s.unwrap().parse().unwrap() })
			.map(|x| x/3-2)
			.map(|x| add_fuel_wt(x))
			.sum();
		println!("Module en brandstof gewicht: {}", wt);
	}
	Ok(())
}

fn add_fuel_wt(wt:i32) -> i32 {
	let mut base = wt;
	let mut sum = 0_i32;
	loop{
		let new_wt = base/3-2;
		if new_wt <= 0 {
			break wt+sum;
		}
		sum += new_wt;
		base = new_wt;
	}
}
