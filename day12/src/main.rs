use std::fmt::Debug;
use std::fmt;

struct Stelsel {
    moons: [Moon;4],    
}

impl Stelsel {
    fn new( x1: i32, y1:i32, z1:i32, 
            x2: i32, y2:i32, z2:i32,
            x3: i32, y3:i32, z3:i32,
            x4: i32, y4:i32, z4:i32,
        ) -> Self {
        Self { moons: [
                Moon::new(x1, y1, z1), 
                Moon::new(x2, y2, z2),
                Moon::new(x3, y3, z3),
                Moon::new(x4, y4, z4) 
                ]}
    }

    // Dtermine the maximum distance between the moons on X, Y and Z
    fn distance(&self) -> (u32, u32, u32) {
        let xmin = Self::min4(self.moons[0].pos.x, self.moons[1].pos.x, self.moons[2].pos.x, self.moons[3].pos.x);
        let xmax = Self::max4(self.moons[0].pos.x, self.moons[1].pos.x, self.moons[2].pos.x, self.moons[3].pos.x);
        let ymin = Self::min4(self.moons[0].pos.y, self.moons[1].pos.y, self.moons[2].pos.y, self.moons[3].pos.y);
        let ymax = Self::max4(self.moons[0].pos.y, self.moons[1].pos.y, self.moons[2].pos.y, self.moons[3].pos.y);
        let zmin = Self::min4(self.moons[0].pos.z, self.moons[1].pos.z, self.moons[2].pos.z, self.moons[3].pos.z);
        let zmax = Self::max4(self.moons[0].pos.z, self.moons[1].pos.z, self.moons[2].pos.z, self.moons[3].pos.z);
        ((xmax-xmin) as u32, (ymax-ymin) as u32, (zmax-zmin) as u32)
    }

    fn min4(a:i32, b:i32, c:i32, d:i32) -> i32 {
        a.min(b).min(c).min(d)
    } 
    fn max4(a:i32, b:i32, c:i32, d:i32) -> i32 {
        a.max(b).max(c).max(d)
    } 
}

#[derive(Debug, PartialEq)]
struct Moon { 
    pos: Point,
    vel: Point,
}

impl Moon {
    fn new( x:i32, y:i32, z:i32 ) -> Self {
        Moon { pos: Point::new(x, y, z), vel: Point::new(0,0,0) }
    }

    fn offset(&mut self) {
        self.pos.x += self.vel.x;
        self.pos.y += self.vel.y;
        self.pos.z += self.vel.z;
    }

    fn energy(&self) -> i32 {
        (self.pos.x.abs() + self.pos.y.abs() + self.pos.z.abs()) * 
        (self.vel.x.abs() + self.vel.y.abs() + self.vel.z.abs())
    }
}

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}

impl Point {
    fn new(x: i32, y:i32, z:i32) -> Self {
        Self { x, y, z }
    }

    fn max_by_coord(&self, other: &Self) -> Self {
        Point::new(self.x.max(other.x), self.y.max(other.y), self.z.max(other.z))
    }
}

// <x= -7, y= -1, z=  6>
// <x=  6, y= -9, z= -9>
// <x=-12, y=  2, z= -7>
// <x=  4, y=-17, z=-12>
fn main() {

    // ===== Day 1: 
    let mut mns = Stelsel::new(-7, -1, 6,
                                6, -9, -9,
                                -12, 2, -7,
                                4, -17, -12,
                                );
    let rounds = 1000;
    do_steps(rounds, &mut mns.moons);
    let energy:i32 = mns.moons.iter().map( |m| m.energy() ).sum();
    println!("Energy after {} round: {}", rounds, energy);

    // ===== Day 2:
    // X, Y and Z are independent.
    // We will measure a period by monitoring the maximum value of the min and max value of all moons
    // If this max distance between moons is repeated we may have a period
    let mns = Stelsel::new(-7,  -1,   6, 
                                6,  -9,  -9,
                              -12,   2,  -7,
                                4, -17, -12,
                            );
    println!("Cycle size: {}", findcycle(mns));
}

struct CycleTest{
    cycle    : Vec<u32>,     // Collect cycle, starting with a maximum
    rnds     : Vec<u32>,     // Collect rounds bewtween maximum 
    max_dist : u32,          // Maximum seen distance
    result   : u32,          // cycle or 0
}

impl CycleTest {
    fn new() -> Self {           
        let mut cycle = Vec::with_capacity(1000);
        cycle.push(0);
        let rnds = vec![0];
        Self { cycle: cycle,
                rnds: rnds,
                max_dist: 0,
                result: 0,
             }
    }

    /// To find a cycle, the start of a cycle must be identified, and its not
    /// necessarily at the start of the first round.
    /// A cycle is assumed to start at a repeatedly found maximum. 
    /// The next items must then be identical.
    /// Task 1 is to identify a maximum. Every time a new max is found it is written to a vector at pos 0.
    /// The following are written to the next positions
    /// If a maximum is found again, it is written to the end of the line, the length noted and 
    /// a comparison is also started for the following numbers. If that holds to the noted length
    /// the cycle length is known.
    fn test_cycle(&mut self, sample : u32, round : u32) -> u32 {
        if self.result > 0 {
            return self.result;
        }
        self.try_cycle(sample, round);
        if self.rnds.len() > 2 {
            let lastr = self.rnds.len()-1;
            let base = self.rnds[0];
            if self.rnds[lastr] - self.rnds[lastr-1] < self.rnds[lastr-1]-self.rnds[lastr-2] {
                self.rnds.pop();
            } else if self.rnds[lastr] - self.rnds[lastr-1] > self.rnds[lastr-1]-self.rnds[lastr-2] {
                self.rnds.remove(lastr-1);
            } else {
                if self.cycle[(self.rnds[lastr-2] - base) as usize .. (self.rnds[lastr-1] - base) as usize] == 
                   self.cycle[(self.rnds[lastr-1] - base)  as usize .. (self.rnds[lastr] - base) as usize ] {
                       return self.rnds[lastr] - self.rnds[lastr-1]
                   }
            }
        }
        0
    }

    fn cyclesize(&self) -> u32 {
        if self.rnds.len() <= 2 {
            0
        } else {
            let lastr = self.rnds.len()-1;
            let base = self.rnds[0];
            if self.rnds[lastr] - self.rnds[lastr-1] == self.rnds[lastr-1]-self.rnds[lastr-2] {
                self.rnds[lastr] - self.rnds[lastr-1]
            } else {
                0
            }
        }
    }

    /// try_cycle sees if the new distance is equal, greater or smaller then the previous maximum
    /// if it is smaller: ignore
    /// if greater: store the round in self.cycle at position 0, and return false
    /// if equal: psuh the round onto the store and return true
    /// The return value indicates that it is useful to test for a cycle
    /// Precondition: store has a value in position 0
    fn try_cycle(&mut self, sample: u32, round: u32) {
        assert!(self.cycle.len() > 0);
        if sample > self.max_dist {
            self.cycle.truncate(1);
            self.cycle[0] = sample;
            self.rnds.truncate(1);
            self.rnds[0] = round;
            self.max_dist = sample;
        } else if sample == self.max_dist {
            self.cycle.push(sample);
            self.rnds.push(round);
        } else {   // sample < max_dist  
            self.cycle.push(sample);
        }
    }
}

impl fmt::Display for CycleTest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {} {}", self.max_dist, self.cyclesize(), self.result)
    }
}


/// Observation: X, Y and Z are independent of each other.
/// So find a cycle for X, Y and Z and find the least common multiple
fn findcycle(mut mns: Stelsel) -> u64 {
    let mut cycletestx = CycleTest::new();
    let mut cycletesty = CycleTest::new();
    let mut cycletestz = CycleTest::new();

    let mut round: u32 = 0;                // counts the current round
    
    loop {
        do_steps(1, &mut mns.moons);
        round += 1;
        let dist = mns.distance();
        // Collect any new max distances, or hold previous max
        let cycx = cycletestx.test_cycle(dist.0, round) as u64;
        let cycy = cycletesty.test_cycle(dist.1, round) as u64;
        let cycz = cycletestz.test_cycle(dist.2, round) as u64;

        print!("{:10}: x: {} y: {} z: {}\r", round, cycletestx, cycletesty, cycletestz);

        let result = lcm3(cycx, cycy, cycz);
        if result > 0 {
            println!("");
            return result
        }
    }
}


fn do_steps(rounds: usize, moons: &mut [Moon;4]) {
    for _ in 0..rounds {
        get_velocities(moons);
        for n in 0..moons.len(){
            moons[n].offset();
        }
    }
}

fn get_velocities(moons: &mut [Moon;4]) {
    let mnlen = moons.len();
    for n1 in 0..mnlen {
        for n2 in 0..mnlen {
            let mut v = Point::new(0,0,0);
            if n1 == n2 { continue; }
            { 
                v.x = moons[n1].vel.x + if  moons[n1].pos.x > moons[n2].pos.x {-1} else
                                        if  moons[n1].pos.x < moons[n2].pos.x { 1} else {0};
                v.y = moons[n1].vel.y + if moons[n1].pos.y > moons[n2].pos.y {-1} else 
                                        if moons[n1].pos.y < moons[n2].pos.y { 1} else {0};
                v.z = moons[n1].vel.z + if moons[n1].pos.z > moons[n2].pos.z {-1} else 
                                        if moons[n1].pos.z < moons[n2].pos.z { 1} else {0}; 
            }
            moons[n1].vel = v;
        }
    }
}

fn lcm3(a: u64, b: u64, c: u64 ) -> u64 {
    lcm(lcm(a,b), c)
}

fn lcm(a: u64, b: u64) -> u64 {
    if a == 0 || b == 0 {
        0
    } else {
        let div = gcd(a, b);
        a * b / div
    }
}

fn gcd(mut a: u64, mut b: u64) -> u64 {
    while b != 0 {
        let tmp = a;
        a = b;
        b = tmp % b;
    }
    a
}


#[cfg(test)]
mod tests {
    use super::*;

    fn start() ->  Stelsel {
        Stelsel::new(-1, 0, 2,
            2, -10, -7,
            4, -8, 8,
            3, 5, -1,
            )
        }

    fn start2() ->  Stelsel {
        Stelsel::new(-8, -10, 0, 
                      5, 5, 10,
                      2, -7, 3,
                      9, -8, -3)
    }

    #[test]
    fn test1() {
        let mut mns = start();
        do_steps(1, &mut mns.moons);
        let mut mns_1 = Stelsel::new(2, -1, 1,
                                     3,-7, -4,
                                     1, -7, 5,
                                     2, 2, 0);
        mns_1.moons[0].vel = Point::new( 3, -1, -1 );
        mns_1.moons[1].vel = Point::new( 1, 3, 3 );
        mns_1.moons[2].vel = Point::new( -3, 1, -3 );
        mns_1.moons[3].vel = Point::new( -1, -3, 1 );
        assert_eq!( mns.moons[0], mns_1.moons[0]);  // left = get, right = expect

        let mut mns = start();
        do_steps(10, &mut mns.moons);
        let mut mns_10 = Stelsel::new(2, 1, -3,
                                        1,-8, 0,
                                        3, -6, 1,
                                        2, 0, 4);
        mns_10.moons[0].vel = Point::new( -3, -2, 1 );
        mns_10.moons[1].vel = Point::new( -1, 1, 3 );
        mns_10.moons[2].vel = Point::new( 3, 2, -3 );
        mns_10.moons[3].vel = Point::new( 1, -1, -1 );
        assert_eq!( mns.moons[0], mns_10.moons[0]);
        assert_eq!( mns.moons[1], mns_10.moons[1]);
        assert_eq!( mns.moons[2], mns_10.moons[2]);
        assert_eq!( mns.moons[3], mns_10.moons[3]);
    }

    #[test]
    fn test2_1() {
        let mut mns = start2();
        do_steps(10, &mut mns.moons);
        let mut mns_10 = Stelsel::new( -9, -10, 1,
                                        4, 10, 9,
                                        8, -10, -3,
                                        5, -10, 3);
        mns_10.moons[0].vel = Point::new( -2, -2, -1 );
        mns_10.moons[1].vel = Point::new( -3, 7, -2 );
        mns_10.moons[2].vel = Point::new( 5, -1, -2 );
        mns_10.moons[3].vel = Point::new( 0, -4, 5 );
        assert_eq!( mns.moons[0], mns_10.moons[0]);
        assert_eq!( mns.moons[1], mns_10.moons[1]);
        assert_eq!( mns.moons[2], mns_10.moons[2]);
        assert_eq!( mns.moons[3], mns_10.moons[3]);
    }

    #[test]
    fn test2_2() {
        let mut mns = start2();
        do_steps(100, &mut mns.moons);
        let mut mns_100 = Stelsel::new( 8, -12, -9,
                                         13, 16, -3,
                                        -29, -11, -1,
                                         16, -13, 23);
        mns_100.moons[0].vel = Point::new( -7, 3, 0 );
        mns_100.moons[1].vel = Point::new( 3, -11, -5 );
        mns_100.moons[2].vel = Point::new( -3, 7, 4 );
        mns_100.moons[3].vel = Point::new(  7, 1, 1 );
        assert_eq!( mns.moons[0], mns_100.moons[0]);
        assert_eq!( mns.moons[1], mns_100.moons[1]);
        assert_eq!( mns.moons[2], mns_100.moons[2]);
        assert_eq!( mns.moons[3], mns_100.moons[3]);
        let energy:i32 = mns.moons.iter().map( |m| m.energy() ).sum();
        assert_eq!(energy, 1940)
    }

}
