use std::fs;

fn main() {
    let input = fs::read_to_string("day8_input.txt").unwrap();
    let tall = 6;
    let wide = 25;

    let numv: Vec<_> = input.trim().chars().map(|c| c.to_digit(10).unwrap()).collect();
    let num_sl = &numv[..];
    let layers :Vec<_> = num_sl.chunks(tall*wide).collect(); 
    // type layers:  Vec<&[u32]>

    let zeronr = layers.iter()
                       .map(|v| v.iter()
                                 .filter(|x| **x == 0)
                                 .count())
                        .enumerate()
                        //.inspect(|(a,b)| println!("{}: {}", a, b))
                        .min_by(|(_,x), (_,y)|x.cmp(y))
                        .unwrap().0;
    println!("{:?}", zeronr);
    let num_ones = layers[zeronr].iter().filter(|x| **x==1).count();
    let num_twos = layers[zeronr].iter().filter(|x| **x==2).count();
    println!("{}", num_ones * num_twos);

    let mut colors : Vec<u32> = vec![];
    for pix in 0..(wide*tall) {
        let stack:  Vec<_> = layers.iter()
                                   .map(|x| x[pix])
                                   .collect();
        colors.push( get_pixel(stack) );
    }
    for (n, c) in colors.iter().enumerate() {
        print!("{}",if *c == 0 {"."} else {"X"});
        if n % wide == wide-1 {
            println!("");
        }
    }
}

fn get_pixel(stack: Vec<u32>) -> u32 {
    for pix in stack{
        if pix != 2 {
            return pix;
        }
    } 
    return 2;
} 