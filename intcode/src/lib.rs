use my_error;

#[derive(PartialEq, Debug)]
struct Opcode {
	opc: i32,
	md1: i32,
	md2: i32,
	md3: i32,
}

impl Opcode {

	// Retrieve the next opcode, and the modes for its operands
	fn new(opcode: i64) -> Self {
		let opc = (opcode % 100) as i32;
		
		let opcode = ((opcode - opc as i64)/100) as i32;
		let md1 = opcode % 10;
		
		let opcode = (opcode - md1)/10;
		let md2 = opcode % 10;
		
		let md3 = (opcode - md2)/10;

		Self{ opc, md1, md2, md3 }
	}
	
}

pub struct Intcode {
	mem: Vec<i64>,		// geheugen
	inputs: Vec<i64>,
	pub outputs: Vec<i64>,
	input_index: usize,	// index to next input to be read
	pc: usize,			// program counter
	rb: usize,			// relative base
	halted: bool,
}


impl Intcode {
	// Create a new machine, with the given program loaded in memeory and the given inputs preloaded
	pub fn new(mem: Vec<i64>, inputs: Vec<i64>) -> Self {
		Intcode{ mem, inputs, outputs: vec![], input_index: 0, pc:0, rb: 0, halted: false }
	}

	// Set the value on address
	pub fn poke(&mut self, address: usize, value: i64) {
		self.expand_mem_to(address);
		self.mem[address] = value;
	}

	// Get the value from address
	pub fn peek(&mut self, address: usize) -> i64 {
		self.expand_mem_to(address);
		self.mem[address]
	}

	// Expand the memory
	fn expand_mem_to(&mut self, address: usize) {
		if address >= self.mem.len() {
			let mut vec0 = vec![0 as i64;address + 1 - self.mem.len()];
			self.mem.append(&mut vec0);
		}
	}

	// Used for 1st intcode assignment:
	// pokes 'noun' and 'verb' to addresses 1 and 2
	pub fn preset(&mut self, noun: i64, verb: i64){
		self.poke(1, noun);
		self.poke(2, verb);
	}

	// Provide an extra input
	pub fn push_in(&mut self, value: i64){
		self.inputs.push(value);
	}

	pub fn is_halted(&self) -> bool {
		self.halted
	}

	// Run the machine
	// It returns Ok(false) if:
	// - the inputs run out
	// It returns Ok(true) if:
	// - the machine executes a halt instruction
	// Once halted, no more instructions are accepted, and Err("Halted") is returned

	pub fn run<'a>(&'a mut self) ->  Result<bool, my_error::MyError> {
		while !self.halted {
			let Opcode {opc, md1, md2, md3 } = Opcode::new(self.peek(self.pc));
			//print!("** pc:{}:{}({},{},{}) ", self.pc, opc, md1, md2, md3);
			if md3 == 1 { panic!{format!("Immediate destionation mode on pc={}", self.pc)}}
			match opc {
				1 => { // addition: oprnd3 := oprnd1 + oprnd2
						//print!("{} {} {}: ", self.mem[self.pc+1], self.mem[self.pc+2], self.mem[self.pc+3]);
						if let Some((src1, src2, dst)) = self.get_3_operands(self.pc+1, md1, md2, md3){
							//println!("add {} + {} = {} --> {}", src1, src2, src1+src2, dst);
							self.poke(dst, src1 + src2);
							self.pc += 4;
					   }	
					 },
				2 => { // multiply: oprnd3 := oprnd1 * oprnd2
					//print!("{} {} {}: ", self.mem[self.pc+1], self.mem[self.pc+2], self.mem[self.pc+3]);
					if let Some((src1, src2, dst)) = self.get_3_operands(self.pc+1, md1, md2, md3){
							//println!("add {} * {} = {} --> {}", src1, src2, src1*src2, dst);
							self.poke(dst, src1 * src2);
							self.pc += 4;
						}	
				 	 },
				3 => { // input : store in oprnd1
					//print!("{} : ", self.mem[self.pc+1]);
					if self.inputs.len() > self.input_index {
							//println!("opcode 3 with mode: {}", md1);
							let dst = self.get_oprnd_addr(self.pc+1, md1).expect("No valid destination");
							//println!{"opr1@{}: {}, rb: {} --> dst: {}", self.pc+1, self.peek(self.pc+1), self.rb, dst};
							//println!("input #{} = {}", self.input_index, self.inputs[self.input_index]);
							self.poke(dst as usize, self.inputs[self.input_index]);
							self.input_index += 1;
							self.pc +=2;
						} else {
							return Ok(false);
						}
					 },
				4 => { // output: write from oprnd1 to outputs
					//print!("{} : ", self.mem[self.pc+1]);
					match self.get_oprnd(self.pc+1, md1) {
							Ok(src) => { //println!("ouptput {}", src);
										 self.outputs.push(src)
										},
							Err(msg) => panic!("Cannot find operand for Output at pc = {}: {}", self.pc, msg)
						}
						self.pc += 2;   
					 },
				5 => { // if oprnd1 <> 0: jump to [oprnd2]
					//print!("{} {}: ", self.mem[self.pc+1], self.mem[self.pc+2]);
					if let Some((src, dst)) = self.get_2_operands(self.pc+1, md1, md2) {
							//print!("jump if {} <> 0", src);
							if src != 0 {
								self.pc = dst;
							} else {
								self.pc += 3;
							}
							//println!("--> {}", self.pc);
						}
					 },
				6 => { // if oprnd1 == 0: jump to [oprnd2]
					//print!("{} {}: ", self.mem[self.pc+1], self.mem[self.pc+2]);
					if let Some((src, dst)) = self.get_2_operands(self.pc+1, md1, md2) {
							//print!("jump to {} if {} == 0", dst, src);
							if src == 0 {
								self.pc = dst;
							} else {
								self.pc += 3;
							}
							//println!("--> {}", self.pc);
						}
					 },
				7 => { // if oprnd1 < oprnd2: [oprnd3] := 1 else 0
					//print!("{} {} {}: ", self.mem[self.pc+1], self.mem[self.pc+2], self.mem[self.pc+3]);
					if let Some((src1, src2, dst)) = self.get_3_operands(self.pc+1, md1, md2, md3) {
							//println!("if {} < {}: {} = 1 else 0", src1, src2, dst);
							self.poke(dst, if src1 < src2 { 1 } else { 0 });
						}
						self.pc += 4;
					},
				8 => { // if oprnd1 == oprnd2: [oprnd3] := 1 else 0
					//print!("{} {} {}: ", self.mem[self.pc+1], self.mem[self.pc+2], self.mem[self.pc+3]);
					if let Some((src1, src2, dst)) = self.get_3_operands(self.pc+1, md1, md2, md3) {
							//println!("if {} == {}: {} = 1 else 0", src1, src2, dst);
							self.poke(dst, if src1 == src2 { 1 } else { 0 });
						}
						self.pc += 4;
					},
				9 => {	// change relative base with oprnd1
					//print!("{} : ", self.mem[self.pc+1]);
					match self.get_oprnd(self.pc+1, md1) {
							Ok(src) if src >= 0 => self.rb += src as usize ,
							Ok(src)       => self.rb -= (-src) as usize, 
							Err(msg) => panic!("Cannot find operand for Output at pc = {}: {}", self.pc, msg)
						}
						//println!("rel. base set to {}", self.rb);
						self.pc += 2;   
					},
			   99 => {  // End of progam
				   		self.halted = true;
				   		return Ok(true);
					 },  
				_ => return Err(my_error::MyError::new(format!("Illegal opcode {} at {}", opc, self.pc))),
			}
			//println!("{}: {:?}/{:?}/{:?}", opc, self.mem, self.inputs,self.outputs);
		}
		Err(my_error::MyError::new(format!("Halted")))  
	}

	// Get three operands, with the indicated modes
	fn get_3_operands(&mut self, pc: usize, md1: i32, md2: i32, md3: i32) -> Option<(i64, i64, usize)> {
		if let ( Ok(src1), Ok(src2), Ok(dst)) = 
			   		( self.get_oprnd(pc, md1),
				 	  self.get_oprnd(pc+1, md2),
				 	  self.get_oprnd_addr(pc+2, md3) ) 
		{
			Some((src1, src2, dst as usize))
		} else {
			None
		}
	}

	// Get 2 opeands, with the indicated modes
	fn get_2_operands(&mut self, pc: usize, md1: i32, md2: i32) -> Option<(i64, usize)> {
		//print!("<oprnd2:{},{},{}:", pc, md1, md2);
		if let (Ok(src1), Ok(dst)) = (self.get_oprnd(pc, md1), 
									  self.get_oprnd(pc+1, md2)) {
			//print!("{}, {}>",src1, dst);
			Some((src1, dst as usize))
		} else {
			None
		}
	}

	// Get a single operand
	fn get_oprnd(&mut self, addr: usize, mode: i32) -> Result<i64, my_error::MyError > {
		self.get_oprnd_addr(addr, mode).map(|adr| self.peek(adr))
	}


	// Get the address of a single operand. Usefull for destinations
	fn get_oprnd_addr( &mut self, addr: usize, mode: i32) -> Result<usize, my_error::MyError> {
		match mode {
			0 => { // position mode: operand indicates address of required value
					Ok(self.peek(addr) as usize)
				},
			1 => { // immediate mode: operand is required value
					Ok(addr)
				},
			2 => { // relative mode: operand indicates offset to relative base for the address
					let a = self.rb;
					let b = self.peek(addr);
					//println!("rel. mode: {} + {} --> {}", a, b, self.peek((a as i64 + b) as usize));
					Ok((a as i64 + b) as usize)
				},
			_ => Err(my_error::MyError::new(format!("Unknown opmode {} for address {}", mode, addr)))
		}
	}
}

#[test]	
fn test_opcode_interpreter() {
	assert_eq!(Opcode::new(12345), Opcode{ opc: 45, md1: 3, md2: 2, md3: 1 });
}