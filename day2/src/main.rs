// Emulate a simple computer machine
// Use it on the input in the file day2_input.txt
// Return lcation 0 when program is ready

use std::fs;

use intcode;

fn main() {
	let input = fs::read_to_string("day2_input.txt").unwrap();
	let list: Vec<i64> = input
					.trim()
					.split(',')
					.map(|s| s.parse::<i64>().unwrap())
					.collect();

	// Day 1

	let mut computer = intcode::Intcode::new(list.clone(), vec![]);
	computer.preset(12, 2);
	let _result = computer.run().unwrap();
	println!("Result for 1202 is: {}", computer.peek(0)); 

	//Day 2

	for noun in 0..100 {
		for verb in 0..100 {
			let mut computer = intcode::Intcode::new(list.clone(), vec![]);
			computer.preset(noun, verb);
			computer.run().unwrap();
			if computer.peek(0) == 19690720_i64 {
				println!("Noun {} and Verb {} produce output {}",
								noun,       verb,          19690720); 
			}
		}
	}
}
