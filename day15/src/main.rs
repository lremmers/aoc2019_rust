use std::fs;
use std::fmt;
use std::convert::{TryFrom, TryInto};
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::collections::VecDeque;

use std::time::{Instant};

use intcode;


#[derive(PartialEq, Debug, Copy, Clone)]
enum FloorStatus {
    Unk = -1,
    Wall,
    Floor,
    Hole,
}

impl TryFrom<i32> for FloorStatus {
    type Error = ();
    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            x if x == FloorStatus::Unk as i32 => Ok(FloorStatus::Unk),
            x if x == FloorStatus::Wall as i32 => Ok(FloorStatus::Wall),
            x if x == FloorStatus::Floor as i32 => Ok(FloorStatus::Floor),
            x if x == FloorStatus::Hole as i32 => Ok(FloorStatus::Hole),
            _ => Err(())
        }
    }
}

impl fmt::Display for FloorStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let symb  = match self {
            Self::Wall => '#',
            Self::Floor => '.',
            Self::Hole => 'X',
            Self::Unk => '?',
        };
        write!(f, "{}", symb)
    }
}

#[derive(Debug)]
struct LocStatus {
    status: FloorStatus,
    distance: u32,
    // n: bool,
    // w: bool,
    // s: bool,
    // e: bool,
}

impl LocStatus {
    fn new() -> Self {
        LocStatus { status: FloorStatus::Unk, distance: u32::MAX}
    }

    fn set_status(&mut self, stat: FloorStatus) {
        self.status = stat
    }
}

impl fmt::Display for LocStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} @ {})", self.status, self.distance)
    }
}


#[derive(Copy, Clone, Debug)]
enum Command {
    Unk = 0,
    North,
    South,
    West,
    East,
}

impl Command {
    pub fn iterator() -> impl Iterator<Item = Command> {
        [Command::North, Command::South, Command::East, Command::West].iter().copied()
    }
}


#[derive(Hash,PartialEq, Debug, Copy, Clone)]
struct Location {
    x: i32,
    y: i32,
}

impl Location {
    fn new(x: i32, y:i32) -> Self {
        Location { x, y }
    }

    fn to_tuple(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    fn to_n(&self) ->  Self {
        Self { x: self.x, y: self.y - 1}
    }
    fn to_s(&self) ->  Self {
        Self { x: self.x, y: self.y + 1}
    }
    fn to_e(&self) ->  Self {
        Self { x: self.x + 1, y: self.y}
    }
    fn to_w(&self) ->  Self {
        Self { x: self.x - 1, y: self.y}
    }
}

impl Eq for Location {}

impl From<(i32, i32)> for Location {
    fn from(xy: (i32, i32)) -> Self {
        Location { x: xy.0, y: xy.1 }
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug)]
struct Map {
    loc: HashMap<Location, LocStatus>,      // Holds known positions with floor status
    x_range: (i32, i32),                    // range of seen x positions    
    y_range: (i32, i32),                    // range of seen y positions
    last_cmd: Command,                      // last issued command
    testing: (i32, i32),                    // next position of the robot if hitting Floor or Hole
    pos: (i32, i32),                        // position of the robot
}

impl Map {
    // Create a new map with a single location at (0,0) and FloorStatus Floor
    fn new(status: FloorStatus) -> Self {
        let mut loc =  HashMap::new();
        let mut stat = LocStatus::new();
        stat.distance = 0;
        stat.set_status(status);
        loc.insert(Location::new(0, 0), stat);
        Map { loc, 
              x_range: (0, 1), 
              y_range: (0, 1),
              last_cmd: Command::Unk,
              testing: (0,0),
              pos: (0,0), 
        }
    }

    // Get a reference to the status at location (x,y), if present
    fn statusxy(& self, pos: (i32, i32)) ->  Option<& LocStatus> {
        self.loc.get(&Location::new(pos.0, pos.1))
    }

    // Get a mutable reference to the status at location (x,y), if present
    fn statusxy_mut(&mut self, pos: (i32, i32)) ->  Option<&mut LocStatus> {
        self.loc.get_mut(& Location::new(pos.0, pos.1))
    }

    // insert a possibly new location and set the status
    // if the location exists already, and its status is Unk, its status is updated
    fn insert(&mut self, pos: (i32, i32), status: FloorStatus){
        let loc = Location::new(pos.0, pos.1);
        let stat = self.loc.entry(loc).or_insert(LocStatus::new());
        if stat.status == FloorStatus::Unk {
            stat.set_status(status)
        }
        self.x_range.0 = self.x_range.0.min(loc.x);  
        self.x_range.1 = self.x_range.1.max(loc.x + 1);  
        self.y_range.0 = self.y_range.0.min(loc.y);  
        self.y_range.1 = self.y_range.1.max(loc.y + 1);  
    }

    // Search for the Hole, using Breadth First Search
    // Starting from the origin, all 4 neighbours are tried. If they are Floor, they are added 
    // the search Queue and to Self. If they are Wall, they are just added to Self.
    // Each node thus is handled like this:
    // 0. Add start to the testing Queue: A FIFO queue
    // While queue not empty
    //      1. Go to the node using Floor items (path_to_xy).
    //          Mark it as visited.
    //      2. For each of the 4 directions:
    //          if not yet visited
    //          a. Test it and add it to Self
    //              1. If it is Wall: Done
    //              2. If is is Hole: Return with the level of the node as he answer
    //              3. If it is Floor: 
    //                  a. Add it to the Testing Queue
    //                  b. Go back to node
    //      3. Remove Node from the testing Queue
    // 
    //     
    fn search_hole(&mut self, start: Location, cmptr: & mut intcode::Intcode) -> Location {
        let mut queue: VecDeque<Location> = VecDeque::with_capacity(self.loc.len());
        let mut visited: HashSet<Location>  = HashSet::with_capacity(self.loc.len());
        let mut cnt  = 0;
        let mut pos_hole = Location::new(0,0);
        queue.push_back(start); 
        while let Some(base) = queue.pop_front() {
            let base_tuple = base.to_tuple();
            self.goto_xy(cmptr, self.path_to_xy_bfs(Location::new(self.pos.0, self.pos.1), base));
            self.pos = base_tuple;
            visited.insert(base);
            let distance = self.loc.get(&base).unwrap().distance;
            for cmd in Command::iterator() {
                if let Some(value) = self.test(cmptr, &mut queue, &visited, base, cmd, distance) { 
                    pos_hole = value
                }
            }
            cnt += 1;
            scr_home();
            println!("Map after round {}\n{}", cnt, self)
        }       
        //println!("Oxygen leak found at {} at distance {}", pos_hole, self.statusxy(pos_hole.to_tuple()).unwrap().distance);
        pos_hole
    }

    //          a. Test it and add it to Self
    //              1. If it is Wall: Done
    //              2. If is is Hole: Return with the level of the node as he answer
    //              3. If it is Floor: 
    //                  a. Add it to the Testing Queue
    //                  b. Go back to node
    fn test(&mut self, cmptr: &mut intcode::Intcode, 
                   queue: &mut VecDeque<Location>,
                   visited: &HashSet<Location>, 
                   base: Location,
                   cmd: Command,
                   distance: u32 ) -> Option<Location> {

        let testpos = match cmd {
            Command::North => base.to_n().to_tuple(),
            Command::South => base.to_s().to_tuple(),
            Command::East  => base.to_e().to_tuple(),
            Command::West  => base.to_w().to_tuple(),
            Command::Unk   => panic!("Command::Unk presented to map.test")
        };
        let testpos_loc = Location::new(testpos.0, testpos.1);
        if visited.contains(&testpos_loc)    {
            return None;
        }
        //print!("Testing ({},{}) from {} using cmd {:?}: ", testpos.0,  testpos.1, base, cmd);
        let output:i32;
        {
            cmptr.push_in(cmd as i64);
            let _result = cmptr.run().unwrap();
            output = cmptr.outputs.pop().unwrap() as i32;
        }
        let status: FloorStatus = output.try_into().unwrap();
        self.insert(testpos, output.try_into().unwrap());
        let mut current_sts = &mut self.statusxy_mut(testpos).unwrap();
        current_sts.distance = current_sts.distance.min(distance + 1);
        //println!("{}", self.statusxy(testpos).expect(&format!("Testing: position {:?} not in map", testpos))); // Also proves the entry exists now, or panic
        if status == FloorStatus::Floor || status == FloorStatus::Hole {
            self.goto_xy(cmptr, self.path_to_xy_bfs(testpos_loc, base));
        }
        let test_loc = Location::new(testpos.0, testpos.1);
        if status ==FloorStatus::Floor {
            queue.push_back(test_loc);
        }
        if status == FloorStatus::Hole {
            queue.push_back(test_loc);
            Some(test_loc)
        } else {
            None
        }
    }

    // Let the computer walk the path
    fn goto_xy(&self, cmptr: &mut intcode::Intcode, path: Vec<Location>){
        if path.len() == 0 {
            return;
        }
        let mut loc0 = path[0];
        //let dest = path[path.len()-1];
        //println!("Walk from ({},{}) to ({},{}), {} steps.", loc0.x, loc0.y, dest.x, dest.y, path.len()-1);  
        for loc in path.iter().skip(1) {
            let translation = (loc.x - loc0.x, loc.y - loc0.y); 
            let cmd = match translation {
                ( 0,  1) => Command::South,
                ( 0, -1) => Command::North,
                ( 1,  0) => Command::East,
                (-1,  0) => Command::West,
                (a, b) => panic!("Translation over ({},{}) required from ({},{}) to ({},{})", a, b, loc0.x, loc0.y, loc.x, loc.y)
            };
            cmptr.push_in(cmd as i64);
            cmptr.run().unwrap();
            cmptr.outputs.pop().unwrap();
            loc0 = *loc;
        }
    }

    // Find the shortest path from 'from' to anywhere in the map
    // Preconditions: the maze is complete
    //                the from location has a Floor or a Hole
    // Used to see how fast things flood (with oxygen)
    fn flood_map(&self, from: Location) -> u32 {

        let map_size = self.loc.len();
        let mut distances: HashMap<Location, u32> = HashMap::with_capacity(map_size);
        for (k, v) in self.loc.iter() {
            if v.status == FloorStatus::Floor || v.status == FloorStatus::Hole { 
                distances.insert(*k, 0);
            }
        }
        // distances now contains all known locations that can be walked on

        let map_size = distances.len();
        let mut queue: VecDeque<Location> = VecDeque::with_capacity(map_size);
        let mut visited: HashSet<Location>  = HashSet::with_capacity(map_size);

        queue.push_back(from);
        while let Some(base) = queue.pop_front() {
            visited.insert(base);
            for cmd in Command::iterator() {
                let testpos = match cmd {
                    Command::North => base.to_n(),
                    Command::South => base.to_s(),
                    Command::East  => base.to_e(),
                    Command::West  => base.to_w(),
                    Command::Unk   => panic!("Command::Unk presented to map.test")
                };
                if !distances.contains_key(&testpos) {
                    continue;   	    // Cannot go here
                    }
                if visited.contains(&testpos){ 
                    continue;           // Already was here
                }
                if distances[&testpos] == 0 {
                    distances.insert(testpos, distances[&base] +  1);
                } 
                queue.push_back(testpos);
            }           
        }
        *distances.values().max().unwrap()                    
    }

    // Find a path from 'from' to 'dest', typically for the computer to walk
    // Use a BFS algo. Only use known locations and break on first hit of the destination
    // Preconditons: from and dest are both in the map.
    //               there is a path between from and dest
    fn path_to_xy_bfs(&self, from: Location, dest: Location) -> Vec<Location> {
        //println!("Finding path from {} to {}", from, dest);
        if from == dest {
            return vec![];
        }
        let map_size = self.loc.len();
        let mut queue: VecDeque<Location> = VecDeque::with_capacity(map_size);
        let mut visited: HashSet<Location>  = HashSet::with_capacity(map_size);
        let mut prev_loc: HashMap<Location, Option<Location>> = HashMap::with_capacity(map_size);
        for (k, v) in self.loc.iter() {
            if v.status == FloorStatus::Floor || v.status == FloorStatus::Hole { 
                prev_loc.insert(*k, None);
            }
        }
        // prev_loc now contains all known locations that can be walked on
        queue.push_back(from); 
        'while_loop:
        while let Some(base) = queue.pop_front() {
            visited.insert(base);
            for cmd in Command::iterator() {
                let testpos = match cmd {
                    Command::North => base.to_n(),
                    Command::South => base.to_s(),
                    Command::East  => base.to_e(),
                    Command::West  => base.to_w(),
                    Command::Unk   => panic!("Command::Unk presented to map.test")
                };
                if !prev_loc.contains_key(&testpos) {
                    continue;   	    // Cannot go here
                    }
                if visited.contains(&testpos){ 
                    continue;           // Already was here
                }
                prev_loc.insert(testpos, Some(base));    // Record previous node
                if testpos == dest {
                    break 'while_loop; // done searching
                }
                queue.push_back(testpos);
            }           
        }            
        if prev_loc[&dest].is_none() {
            panic!("No path from {} to {} found");
        }
        let mut current = dest;
        let mut result: Vec<Location> = vec![];
        while current != from {
            let v = prev_loc[&current].unwrap();  // We know this entry exists
            result.push(current);
            current = v;
        }
        result.push(from);    
        result.reverse();   // yields no value
        //println!("{:?}", result); 
        result   
    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in self.y_range.0..self.y_range.1 {
            for x in self.x_range.0..self.x_range.1 {
                match (x, y) {
                  (0,0) => write!(f, "O")?,
                  (a, b) if (a,b) == self.pos => write!(f, "●")?,
                  (a, b) => {
                      if let Some(value) = self.statusxy((a,b)) { 
                        write!(f, "{}", value.status)?;
                      } else {
                        write!(f, " ")?; 
                      }
                    },
                }
            }
            writeln!(f, "")?;
        }
    Ok(())
    }
}

fn main() {
    let input = fs::read_to_string("day15_input.txt").unwrap();
    let program: Vec<i64> = input
                    .trim()
                    .split(',')
                    .enumerate()
                    .map(|(i,s)| s.parse().expect(&format!("Failed at {}: '{}'", i, s)))
                    .collect();
    let mut computer = intcode::Intcode::new(program.clone(), vec![]);

    let mut map = Map::new(FloorStatus::Floor);
    let start = Instant::now();
    let loc_hole = map.search_hole(Location::new(0,0), &mut computer);
    let lapse = start.elapsed();
    println!("Elapsed:   {}", lapse.as_secs_f32());
    let distance = map.loc.get(&loc_hole).unwrap().distance;
    println!("Hole at {} is {} steps away", loc_hole, distance);
    let minutes = map.flood_map(loc_hole);
    println!("Reflooding completed in {} minutes", minutes)
}

fn scr_home() {
    print!("\x1B[2J"); // cleaer
    print!("\x1B[;H"); // top left
}
