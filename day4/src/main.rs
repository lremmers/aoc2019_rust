fn main() {
    let input0 = 178416;
    let input1 = 676461;
    let result = (input0..=input1)
                        .filter(|x| is_code(x))
                        .count();
    println!("Number of correct codes: {}", result);
    let result = (input0..=input1)
                        .filter(|x| is_code(x))
                        .filter(|x| is_code2(x))
                        .count();
    println!("Number of correct codes: {}", result);
}

fn is_code(code: &i32) -> bool {
    let digits: Vec<_> = code.to_string().chars().collect();
    if digits.len() != 6 {
        return false;
    }
    if digits[..5]
        .iter()
        .enumerate()
        .all(|(i, x)| digits[i + 1] != *x)
    {
        return false;
    }
    if digits[..5]
        .iter()
        .enumerate()
        .any(|(i, x)| digits[i + 1] < *x) 
    {
        return false;
    }
    //println!("{}", code);
    true
}

fn is_code2(code: &i32) ->  bool {
    let digits: Vec<_> = code.to_string().chars().collect();
    if digits[..5]
        .iter()
        .enumerate()
        .any(|(i, x)| (i == 0  || digits[i-1] != *x) && digits[i + 1] == *x && (i == 4 || digits[i+2] != *x))
    {
        //println!("{}", code);
        return true
    }
    false
}